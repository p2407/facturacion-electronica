package log;

import java.io. * ;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class Informe {
    static Properties propiedades = new Properties();
    static String inicialArchivo = "facturacionElectronicaLog_";
    static String extencionArchivo = ".log";
    
    //nuestro archivo log
    public static void InfoLog(String Operacion)throws IOException {
        try {
            propiedades.load(new FileReader("conf/datos_usuario_DB.properties"));
        } catch (IOException ex) {
            try {
                Informe.ErrorLog("Error al obtener las propiedades datos_usuario_DB, motivo: " + ex);
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
            try {
                Informe.ErrorLog("Error, motivo: " + ex);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
            try {
                Informe.ErrorLog("Error, motivo: " + ex);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String dir = propiedades.get("ubicacionLog").toString();
        FileWriter archivo;
        
        Calendar fechaActual = Calendar.getInstance(); //Para poder utilizar el paquete calendar
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        DateFormat df2 = new SimpleDateFormat("yyyyMMdd");
        String reportDate = df.format(fechaActual.getTime());
        String ArchivoDate = df2.format(fechaActual.getTime());
		//Pregunta el archivo existe, caso contrario crea uno con el nombre que le demos
        String nombreArchivo = inicialArchivo + ArchivoDate + extencionArchivo;
        if (new File(dir + "//" + nombreArchivo ).exists() == false) {
            archivo = new FileWriter(new File(dir + "//" + nombreArchivo), false);
        }
        archivo = new FileWriter(new File(dir + "//" + nombreArchivo), true);
        //Empieza a escribir en el archivo
        String informe = "[" + reportDate + "]" + "[INFO]" + " " + Operacion + "\r\n";
        System.out.println(informe);
        archivo.write(informe);
        archivo.close(); //Se cierra el archivo
    } //Fin del metodo InfoLog

    public static void ErrorLog(String Operacion)throws IOException {
        try {
            propiedades.load(new FileReader("conf/datos_usuario_DB.properties"));
        } catch (IOException ex) {
            try {
                Informe.ErrorLog("Error al obtener las propiedades datos_usuario_DB, motivo: " + ex);
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
            try {
                Informe.ErrorLog("Error, motivo: " + ex);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
            try {
                Informe.ErrorLog("Error, motivo: " + ex);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String dir = propiedades.get("ubicacionLog").toString();
        FileWriter archivo;
        
        Calendar fechaActual = Calendar.getInstance(); //Para poder utilizar el paquete calendar
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        DateFormat df2 = new SimpleDateFormat("yyyyMMdd");
        String reportDate = df.format(fechaActual.getTime());
        String ArchivoDate = df2.format(fechaActual.getTime());
		//Pregunta el archivo existe, caso contrario crea uno con el nombre que le demos
        String nombreArchivo = inicialArchivo + ArchivoDate + extencionArchivo;
        if (new File(dir + "//" + nombreArchivo).exists() == false) {
            archivo = new FileWriter(new File(dir + "//" + nombreArchivo), false);
        }
        archivo = new FileWriter(new File(dir + "//" + nombreArchivo), true);
        //Empieza a escribir en el archivo
        String informe = "[" + reportDate + "]" + "[ERROR]" + " " + Operacion + "\r\n";
        System.out.println(informe);
        archivo.write(informe);
        archivo.close(); //Se cierra el archivo
    } //Fin del metodo InfoLog
}
