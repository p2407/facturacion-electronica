package log;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class writeXMLFile {
    static Properties propiedades = new Properties();
    static String extencionArchivo = ".xml";

    // nuestro archivo xml
    public static void saveXML(String Operacion, String fileName, String tipo) throws IOException {
        try {
            propiedades.load(new FileReader("conf/datos_usuario_DB.properties"));
        } catch (IOException ex) {
            try {
                Informe.ErrorLog("Error al obtener las propiedades datos_usuario_DB, motivo: " + ex);
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
            try {
                Informe.ErrorLog("Error, motivo: " + ex);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
            try {
                Informe.ErrorLog("Error, motivo: " + ex);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String dir = propiedades.get("XML_logs").toString() + "//" + tipo;
        File directory = new File(dir);
        if (directory.exists() == false) {
            boolean res = directory.mkdir();
            if (res) {
                System.out.println("El directorio ha sido creado.");
            } else {
                System.out.println("El directorio ya existe.");
            }
        }
        FileWriter archivo;

        Calendar fechaActual = Calendar.getInstance(); // Para poder utilizar el paquete calendar
        DateFormat df2 = new SimpleDateFormat("yyyyMMdd");
        String ArchivoDate = df2.format(fechaActual.getTime());
        dir += "//" + ArchivoDate;
        directory = new File(dir);
        if (directory.exists() == false) {
            boolean res = directory.mkdir();
            if (res) {
                System.out.println("El directorio ha sido creado.");
            } else {
                System.out.println("El directorio ya existe.");
            }
        }
        // Pregunta el archivo existe, caso contrario crea uno con el nombre que le
        // demos
        String nombreArchivo = dir + "//" + fileName + extencionArchivo;
        if (new File(nombreArchivo).exists() == false) {
            archivo = new FileWriter(new File(nombreArchivo), false);
        }
        archivo = new FileWriter(new File(nombreArchivo), true);
        // Empieza a escribir en el archivo
        System.out.println(Operacion);
        archivo.write(Operacion);
        archivo.close(); // Se cierra el archivo
    } // Fin del metodo saveXML

}
