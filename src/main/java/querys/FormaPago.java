package querys;

import java.math.BigDecimal;

import com.roshka.sifen.core.types.CMondT;

public class FormaPago {
	private BigDecimal monto;
	
	private short formaPago;
	
	private String formaPagoDesc;
	
	private short moneCodigo;
	
    private String dNumCheq;
	
    private String dBcoEmi;

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public short getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(short formaPago) {
		this.formaPago = formaPago;
	}

	public String getFormaPagoDesc() {
		return formaPagoDesc;
	}

	public void setFormaPagoDesc(String formaPagoDesc) {
		this.formaPagoDesc = formaPagoDesc;
	}

	public CMondT getMoneCodigo() {
		CMondT retorno = null;
		switch (moneCodigo) {
        case 1:
        	retorno = CMondT.PYG;
        	break;
        case 2:
        	retorno = CMondT.USD;
        	break;
        }
		return retorno;
	}

	public void setMoneCodigo(short moneCodigo) {
		this.moneCodigo = moneCodigo;
	}

	public String getdNumCheq() {
		return dNumCheq;
	}

	public void setdNumCheq(String dNumCheq) {
		this.dNumCheq = dNumCheq;
	}

	public String getdBcoEmi() {
		return dBcoEmi;
	}

	public void setdBcoEmi(String dBcoEmi) {
		this.dBcoEmi = dBcoEmi;
	}

}
