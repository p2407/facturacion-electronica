package querys;

import java.math.BigDecimal;
import java.util.List;

import com.roshka.sifen.core.types.CMondT;

public class ParametrosFacturasNexoQuery {
    private String faelCDC;
    
    private int dNumTim;

    private String dEst;

    private String dPunExp;

    private String dNumDoc;

    private String dSerieNum;

    private String dFeIniT;
    
    private Long moneCodigo;
    
    private String dRucEm;
    
    private String dDVEmi;
    
    private String dNomEmi;
    
    private String dDirEmi;
    
    private String dNumCas;
    
    private String dDesCiuEmi;
    
    private String dTelEmi;
    
    private String dEmailE;
    
    private String dNumIDRec;
    
    private String dNomRec;
    
    private Short iCondOpe;
    
    private int cMoneTiPag;
    
    private String dDirRec;
    
    private int dNumCasRec;
    
    private String dTelRec;
    
    private String dEmailRec;
    
    private String dRucRec;

	private short dDVRec;
    
    private String cDepRec;
    
    private String dFeEmiDE;

	private short iTipEmi;

	private short iTipCont;

	private String dCodSeg;

	private String dDVId;

	private short iTiDE;
	
	private BigDecimal dTiCam;

	private Short iMotEmi;

	private Short iTipDocAso;

	private String dCdCDERef;

	private String dNTimDI;

	private String dEstDocAso;

	private String dPExpDocAso;

	private String dNumDocAso;

	private short iTipoDocAso;

	private String dFecEmiDI;
    
    private DetalleFacturaQuery DetalleFacturaQuery;
    
    private List<ActividadesEconomicasNexoQuery> ActividadesEconomicasNexoQuery;
    
    private short iTipTra;

	private short iNatRec;

	private short iTiContRec;
	
	private String dCelRec;

	private String dVencCuo;

	private BigDecimal dMonCuota;

	public String getFaelCDC() {
		return faelCDC;
	}

	public void setFaelCDC(String faelCDC) {
		this.faelCDC = faelCDC;
	}

	public int getdNumTim() {
		return dNumTim;
	}

	public void setdNumTim(int dNumTim) {
		this.dNumTim = dNumTim;
	}

	public String getdEst() {
		return dEst;
	}

	public void setdEst(String dEst) {
		this.dEst = dEst;
	}

	public String getdPunExp() {
		return dPunExp;
	}

	public void setdPunExp(String dPunExp) {
		this.dPunExp = dPunExp;
	}

	public String getdNumDoc() {
		return dNumDoc;
	}

	public void setdNumDoc(String dNumDoc) {
		this.dNumDoc = dNumDoc;
	}

	public String getdSerieNum() {
		return dSerieNum;
	}

	public void setdSerieNum(String dSerieNum) {
		this.dSerieNum = dSerieNum;
	}

	public String getdFeIniT() {
		return dFeIniT;
	}

	public void setdFeIniT(String dFeIniT) {
		this.dFeIniT = dFeIniT;
	}

	public Long getMoneCodigo() {
		return moneCodigo;
	}

	public void setMoneCodigo(Long moneCodigo) {
		this.moneCodigo = moneCodigo;
	}

	public String getdRucEm() {
		return dRucEm;
	}

	public void setdRucEm(String dRucEm) {
		this.dRucEm = dRucEm;
	}

	public String getdDVEmi() {
		return dDVEmi;
	}

	public void setdDVEmi(String dDVEmi) {
		this.dDVEmi = dDVEmi;
	}

	public String getdNomEmi() {
		return dNomEmi;
	}

	public void setdNomEmi(String dNomEmi) {
		this.dNomEmi = dNomEmi;
	}

	public String getdDirEmi() {
		return dDirEmi;
	}

	public void setdDirEmi(String dDirEmi) {
		this.dDirEmi = dDirEmi;
	}

	public String getdNumCas() {
		return dNumCas;
	}

	public void setdNumCas(String dNumCas) {
		this.dNumCas = dNumCas;
	}

	public String getdDesCiuEmi() {
		return dDesCiuEmi;
	}

	public void setdDesCiuEmi(String dDesCiuEmi) {
		this.dDesCiuEmi = dDesCiuEmi;
	}

	public String getdTelEmi() {
		return dTelEmi;
	}

	public void setdTelEmi(String dTelEmi) {
		this.dTelEmi = dTelEmi;
	}

	public String getdEmailE() {
		return dEmailE;
	}

	public void setdEmailE(String dEmailE) {
		this.dEmailE = dEmailE;
	}

	public String getdNumIDRec() {
		return dNumIDRec;
	}

	public void setdNumIDRec(String dNumIDRec) {
		this.dNumIDRec = dNumIDRec;
	}

	public String getdNomRec() {
		return dNomRec;
	}

	public void setdNomRec(String dNomRec) {
		this.dNomRec = dNomRec;
	}

	public Short getiCondOpe() {
		return iCondOpe;
	}

	public void setiCondOpe(Short iCondOpe) {
		this.iCondOpe = iCondOpe;
	}

	public CMondT getcMoneTiPag() {
		CMondT retorno = null;
		switch (cMoneTiPag) {
        case 1:
        	retorno = CMondT.PYG;
        	break;
        case 2:
        	retorno = CMondT.USD;
        	break;
        }
		return retorno;
	}

	public void setcMoneTiPag(int cMoneTiPag) {
		this.cMoneTiPag = cMoneTiPag;
	}

	public String getdDirRec() {
		return dDirRec;
	}

	public void setdDirRec(String dDirRec) {
		this.dDirRec = dDirRec;
	}

	public int getdNumCasRec() {
		return dNumCasRec;
	}

	public void setdNumCasRec(int dNumCasRec) {
		this.dNumCasRec = dNumCasRec;
	}

	public String getdTelRec() {
		return dTelRec;
	}

	public void setdTelRec(String dTelRec) {
		this.dTelRec = dTelRec;
	}

	public String getdEmailRec() {
		return dEmailRec;
	}

	public void setdEmailRec(String dEmailRec) {
		this.dEmailRec = dEmailRec;
	}

	public String getdRucRec() {
		return dRucRec;
	}

	public void setdRucRec(String dRucRec) {
		this.dRucRec = dRucRec;
	}

	public short getdDVRec() {
		return dDVRec;
	}

	public void setdDVRec(short dDVRec) {
		this.dDVRec = dDVRec;
	}

	public String getcDepRec() {
		return cDepRec;
	}

	public void setcDepRec(String cDepRec) {
		this.cDepRec = cDepRec;
	}

	public String getdFeEmiDE() {
		return dFeEmiDE;
	}

	public void setdFeEmiDE(String dFeEmiDE) {
		this.dFeEmiDE = dFeEmiDE;
	}

	public short getiTipEmi() {
		return iTipEmi;
	}

	public void setiTipEmi(short iTipEmi) {
		this.iTipEmi = iTipEmi;
	}

	public short getiTipCont() {
		return iTipCont;
	}

	public void setiTipCont(short iTipCont) {
		this.iTipCont = iTipCont;
	}

	public String getdCodSeg() {
		return dCodSeg;
	}

	public void setdCodSeg(String dCodSeg) {
		this.dCodSeg = dCodSeg;
	}

	public String getdDVId() {
		return dDVId;
	}

	public void setdDVId(String dDVId) {
		this.dDVId = dDVId;
	}

	public short getiTiDE() {
		return iTiDE;
	}

	public void setiTiDE(short iTiDE) {
		this.iTiDE = iTiDE;
	}

	public BigDecimal getdTiCam() {
		return dTiCam;
	}

	public void setdTiCam(BigDecimal dTiCam) {
		this.dTiCam = dTiCam;
	}

	public Short getiMotEmi() {
		return iMotEmi;
	}

	public void setiMotEmi(Short iMotEmi) {
		this.iMotEmi = iMotEmi;
	}

	public Short getiTipDocAso() {
		return iTipDocAso;
	}

	public void setiTipDocAso(Short iTipDocAso) {
		this.iTipDocAso = iTipDocAso;
	}

	public String getdCdCDERef() {
		return dCdCDERef;
	}

	public void setdCdCDERef(String dCdCDERef) {
		this.dCdCDERef = dCdCDERef;
	}

	public String getdNTimDI() {
		return dNTimDI;
	}

	public void setdNTimDI(String dNTimDI) {
		this.dNTimDI = dNTimDI;
	}

	public String getdEstDocAso() {
		return dEstDocAso;
	}

	public void setdEstDocAso(String dEstDocAso) {
		this.dEstDocAso = dEstDocAso;
	}

	public String getdPExpDocAso() {
		return dPExpDocAso;
	}

	public void setdPExpDocAso(String dPExpDocAso) {
		this.dPExpDocAso = dPExpDocAso;
	}

	public String getdNumDocAso() {
		return dNumDocAso;
	}

	public void setdNumDocAso(String dNumDocAso) {
		this.dNumDocAso = dNumDocAso;
	}

	public short getiTipoDocAso() {
		return iTipoDocAso;
	}

	public void setiTipoDocAso(short iTipoDocAso) {
		this.iTipoDocAso = iTipoDocAso;
	}

	public String getdFecEmiDI() {
		return dFecEmiDI;
	}

	public void setdFecEmiDI(String dFecEmiDI) {
		this.dFecEmiDI = dFecEmiDI;
	}

	public DetalleFacturaQuery getDetalleFacturaQuery() {
		return DetalleFacturaQuery;
	}

	public void setDetalleFacturaQuery(DetalleFacturaQuery detalleFacturaQuery) {
		DetalleFacturaQuery = detalleFacturaQuery;
	}

	public List<ActividadesEconomicasNexoQuery> getActividadesEconomicasNexoQuery() {
		return ActividadesEconomicasNexoQuery;
	}

	public void setActividadesEconomicasNexoQuery(List<ActividadesEconomicasNexoQuery> actividadesEconomicasNexoQuery) {
		ActividadesEconomicasNexoQuery = actividadesEconomicasNexoQuery;
	}

	public short getiTipTra() {
		return iTipTra;
	}

	public void setiTipTra(short iTipTra) {
		this.iTipTra = iTipTra;
	}

	public short getiNatRec() {
		return iNatRec;
	}

	public void setiNatRec(short iNatRec) {
		this.iNatRec = iNatRec;
	}

	public short getiTiContRec() {
		return iTiContRec;
	}

	public void setiTiContRec(short iTiContRec) {
		this.iTiContRec = iTiContRec;
	}

	public String getdCelRec() {
		return dCelRec;
	}

	public void setdCelRec(String dCelRec) {
		this.dCelRec = dCelRec;
	}

	public String getdVencCuo() {
		return dVencCuo;
	}

	public void setdVencCuo(String dVencCuo) {
		this.dVencCuo = dVencCuo;
	}

	public BigDecimal getdMonCuota() {
		return dMonCuota;
	}

	public void setdMonCuota(BigDecimal dMonCuota) {
		this.dMonCuota = dMonCuota;
	}
    
}
