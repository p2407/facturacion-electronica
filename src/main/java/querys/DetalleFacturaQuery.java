package querys;

import java.util.List;

public class DetalleFacturaQuery {
	private List<DetalleDetalleFacturaQuery> detalle;
	
	private List<FormaPago> formaPago;

	public List<DetalleDetalleFacturaQuery> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<DetalleDetalleFacturaQuery> detalle) {
		this.detalle = detalle;
	}

	public List<FormaPago> getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(List<FormaPago> formaPago) {
		this.formaPago = formaPago;
	}    
	
}
