package querys;

public class LotesSifenQuery {
	
	private Long losiCodigo;
	
	private String faelCDC;
	
	private String dProtConsLote;
	
	private String dCodRes;
	
    private String dMsgRes;

	public Long getLosiCodigo() {
		return losiCodigo;
	}

	public void setLosiCodigo(Long losiCodigo) {
		this.losiCodigo = losiCodigo;
	}

	public String getFaelCDC() {
		return faelCDC;
	}

	public void setFaelCDC(String faelCDC) {
		this.faelCDC = faelCDC;
	}

	public String getdProtConsLote() {
		return dProtConsLote;
	}

	public void setdProtConsLote(String dProtConsLote) {
		this.dProtConsLote = dProtConsLote;
	}

	public String getdCodRes() {
		return dCodRes;
	}

	public void setdCodRes(String dCodRes) {
		this.dCodRes = dCodRes;
	}

	public String getdMsgRes() {
		return dMsgRes;
	}

	public void setdMsgRes(String dMsgRes) {
		this.dMsgRes = dMsgRes;
	}

}
