package querys;

import java.math.BigDecimal;

import com.roshka.sifen.core.types.TiAfecIVA;

public class DetalleDetalleFacturaQuery {
	
	private String dCodInt;
	
	private String dDesProSer;

	private BigDecimal dPUniProSer;
	
	private int iAfecIVA;
	
	private BigDecimal dPropIVA;
	
    private BigDecimal dTasaIVA;

	public String getdCodInt() {
		return dCodInt;
	}

	public void setdCodInt(String dCodInt) {
		this.dCodInt = dCodInt;
	}

	public String getdDesProSer() {
		return dDesProSer;
	}

	public void setdDesProSer(String dDesProSer) {
		this.dDesProSer = dDesProSer;
	}

	public BigDecimal getdPUniProSer() {
		return dPUniProSer;
	}

	public void setdPUniProSer(BigDecimal dPUniProSer) {
		this.dPUniProSer = dPUniProSer;
	}

	public TiAfecIVA getiAfecIVA() {
		TiAfecIVA retorno = null;
		switch (iAfecIVA) {
        case 0:
        	retorno = TiAfecIVA.GRAVADO;
        	break;
        case 1:
        	retorno = TiAfecIVA.EXENTO;
        	break;
        }
		return retorno;
	}

	public void setiAfecIVA(int iAfecIVA) {
		this.iAfecIVA = iAfecIVA;
	}

	public BigDecimal getdPropIVA() {
		return dPropIVA;
	}

	public void setdPropIVA(BigDecimal dPropIVA) {
		this.dPropIVA = dPropIVA;
	}

	public BigDecimal getdTasaIVA() {
		return dTasaIVA;
	}

	public void setdTasaIVA(BigDecimal dTasaIVA) {
		this.dTasaIVA = dTasaIVA;
	}

}
