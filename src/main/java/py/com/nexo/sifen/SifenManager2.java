package py.com.nexo.sifen;

import com.google.gson.Gson;
import com.roshka.sifen.Sifen;
import com.roshka.sifen.core.SifenConfig;
import com.roshka.sifen.core.beans.DocumentoElectronico;
import com.roshka.sifen.core.beans.response.RespuestaConsultaLoteDE;
//import com.roshka.sifen.core.beans.response.RespuestaConsultaRUC;
import com.roshka.sifen.core.beans.response.RespuestaRecepcionLoteDE;
import com.roshka.sifen.core.exceptions.SifenException;
//import com.roshka.sifen.internal.util.SifenUtil;

import conexion.ObtenerDatosFacturasOracle;
import log.Informe;
import querys.ActividadesEconomicasNexoQuery;
import querys.ConsultaLotesSifenEntregadosQuery;
import querys.LotesSifenQuery;
import querys.ParametrosFacturasNexoQuery;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SifenManager2 {
    
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SifenManager2.class);

    private SifenManager2(SifenConfig sifenConfig) throws IOException {
        try {
			Sifen.setSifenConfig(sifenConfig);
		} catch (SifenException e) {
			// TODO Auto-generated catch block
			Informe.ErrorLog("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
		}
    }
    
    private void doCheckSifenConfig() throws IOException {
        logger.info("Checking SIFEN CONFIG");
        final SifenConfig sifenConfig = Sifen.getSifenConfig();
        if (sifenConfig != null) {
            logger.info(sifenConfig.toString());
	    	try {
				Informe.InfoLog(sifenConfig.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
        } else {
            logger.error("SifenManager2 won't work. MUST SET SIFEN CONFIG FIRST");
	    	try {
				Informe.ErrorLog("SifenManager2 won't work. MUST SET SIFEN CONFIG FIRST");
			} catch (IOException e) {
				e.printStackTrace();
			}
        }

    }

    private void start() throws IOException {

        logger.info("======== ==== ===== ==========");
        logger.info("Starting NEXO SIFEN MANAGER...");
        logger.info("======== ==== ===== ==========");
        
    	Informe.InfoLog("======== ==== ===== ==========");
    	Informe.InfoLog("Starting NEXO SIFEN MANAGER...");
    	Informe.InfoLog("======== ==== ===== ==========");

        doCheckSifenConfig();

        // PRUEBA CONSULTA RUC
        /*
        try {
            
            final RespuestaConsultaRUC respuestaConsultaRUC = Sifen.consultaRUC("5959208");
            logger.info("Consulta: " + respuestaConsultaRUC.getdCodRes());
            Informe.InfoLog("Consulta: " + new Gson().toJson(respuestaConsultaRUC));
        } catch (SifenException e) {
            Informe.ErrorLog("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
            logger.error("Hubo un error para la consulta en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
        }
        */

        String dProtConsLote = null;

        // PRUEBA ENV�O LOTE

		Informe.InfoLog("****COMIENZA EL ENVIO DE LOS LOTES ACTIVOS A SIFEN****");

        List<LotesSifenQuery> lotesSifenActivos = null;
        
        try {
			lotesSifenActivos = ObtenerDatosFacturasOracle.cargaDatosLotesSifen();
		} catch (SQLException ex) {
			try {
				Informe.ErrorLog("Error en la consulta de los lotes activos, motivo: " + ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
        
        if (lotesSifenActivos != null && lotesSifenActivos.size() > 0) {
        	//Se le llama una vez cada que corre el programa, para no sobrecargar debalde la base
        	List<ActividadesEconomicasNexoQuery> ActividadesEconomicas = ObtenerDatosFacturasOracle.cargaDatosActividadesEconomicasNexo();
        	
        	for (int lsa = 0; lsa < lotesSifenActivos.size(); lsa++) {
        	//for (int lsa = 0; lsa < 1; lsa++) { //Enviar un solo lote por vez
		        try {
		            List<DocumentoElectronico> lote = new ArrayList<>();
					List<ParametrosFacturasNexoQuery> facturas = null;
					try {
						facturas = ObtenerDatosFacturasOracle.cargaDatosFacturacionElectronica(lotesSifenActivos.get(lsa).getLosiCodigo());
					} catch (SQLException ex) {
						try {
							Informe.ErrorLog("Error, motivo: " + ex.getMessage());
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					//Loop del lote de facturas
					for (int i = 0; i < facturas.size(); i++) {
						facturas.get(i).setActividadesEconomicasNexoQuery(ActividadesEconomicas);
					    lote.add(FacturasNexo2.facturaTest01(facturas.get(i)));
					}
		
					Informe.InfoLog("Datos del Lote en crudo: " + new Gson().toJson(facturas));
					Informe.InfoLog("Datos del Lote formateado: " + new Gson().toJson(lote));
			    	
		            final RespuestaRecepcionLoteDE respuestaRecepcionLoteDE = Sifen.recepcionLoteDE(lote);
			    	Informe.InfoLog("Respuesta: " + new Gson().toJson(respuestaRecepcionLoteDE));
		            logger.info("Código de respuesta: " + respuestaRecepcionLoteDE.getdCodRes() + " - Fecha: " + respuestaRecepcionLoteDE.getdFecProc());
	                
	                Informe.InfoLog("Codigo de Lote: " + respuestaRecepcionLoteDE.getdProtConsLote() + " Codigo de Respuesta: *" + respuestaRecepcionLoteDE.getdCodRes() + "* Mensaje de Respuesta: " + respuestaRecepcionLoteDE.getdMsgRes());
			    	
			    	Informe.InfoLog("RAW ANSWER " + respuestaRecepcionLoteDE.getRespuestaBruta());
		            logger.info("RAW ANSWER " + respuestaRecepcionLoteDE.getRespuestaBruta());
		        	
		        	lotesSifenActivos.get(lsa).setdProtConsLote(respuestaRecepcionLoteDE.getdProtConsLote());
		        	lotesSifenActivos.get(lsa).setdCodRes(respuestaRecepcionLoteDE.getdCodRes());
		        	lotesSifenActivos.get(lsa).setdMsgRes(respuestaRecepcionLoteDE.getdMsgRes());
		
		        } catch (SifenException e) {
			    	Informe.ErrorLog("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
		            logger.error("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
		        }
		        if(lotesSifenActivos.get(lsa).getdCodRes() != null) {
			        try {
						ObtenerDatosFacturasOracle.actualizaDatosLotesSifen(lotesSifenActivos.get(lsa).getLosiCodigo(), lotesSifenActivos.get(lsa).getdCodRes(), lotesSifenActivos.get(lsa).getdMsgRes(), lotesSifenActivos.get(lsa).getdProtConsLote());
					} catch (SQLException ex) {
						try {
							Informe.ErrorLog("Error en la actualizacion de los datos del lotes, motivo: " + ex.getMessage());
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
		        }else {
		        	Informe.ErrorLog("********** POSIBLE ERROR EN EL SISTEMA DE LA SET **********");
		        }
	        }
        }else {
        	Informe.InfoLog("No existen lotes activos.");
        }

		Informe.InfoLog("****COMIENZA LA CONSULTA DEL ESTADO DE LOS LOTES ENTREGADOS****");
        
        List<ConsultaLotesSifenEntregadosQuery> dProtConsLoteDetalle = null;
		try {
			dProtConsLoteDetalle = ObtenerDatosFacturasOracle.consultaLotesSifenEntregados();
		} catch (SQLException ex) {
			try {
				Informe.ErrorLog("Error en la consulta de los lotes activos, motivo: " + ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(dProtConsLoteDetalle != null && dProtConsLoteDetalle.size() > 0) {
			for (int i = 0; i < dProtConsLoteDetalle.size(); i++) {
		        dProtConsLote = null;
		        dProtConsLote = dProtConsLoteDetalle.get(i).getdProtConsLote();
		        
		        if (dProtConsLote != null) {
		            logger.info("Consultando LOTE - " + dProtConsLote);
			    	Informe.InfoLog("Consultando LOTE - " + dProtConsLote);
	
		            try {
		                final RespuestaConsultaLoteDE respuestaConsultaLoteDE = Sifen.consultaLoteDE(dProtConsLote);
		                logger.info("Respuesta: " + respuestaConsultaLoteDE.getdCodRes());
				    	Informe.InfoLog("Estado de la consulta: " + respuestaConsultaLoteDE.getCodigoEstado() + " Codigo de Respuesta: *" + respuestaConsultaLoteDE.getdCodResLot() + "* Descripcion: " + respuestaConsultaLoteDE.getdMsgResLot());
				    	
				    	String dCodResLot = respuestaConsultaLoteDE.getdCodResLot();
				    	
		                if(dCodResLot != null && dCodResLot.equals("0362")) { //(Procesamiento de lote concluido)
		                    Informe.InfoLog(new Gson().toJson(respuestaConsultaLoteDE.getgResProcLoteList()));
		    		        try {
		    					ObtenerDatosFacturasOracle.actualizaLotesEntregadosSifen(dProtConsLote, respuestaConsultaLoteDE.getgResProcLoteList());
		    				} catch (SQLException ex) {
		    					try {
		    						Informe.ErrorLog("Error en la actualizacion de los datos del lotes: " +dProtConsLote+ ", motivo: " + ex.getMessage());
		    					} catch (IOException e) {
		    						e.printStackTrace();
		    					}
		    				}
		                }
						if (dCodResLot == null){
							Informe.ErrorLog("********** POSIBLE ERROR EN EL SISTEMA DE LA SET **********");
						}
		            } catch (SifenException e) {
		    	    	Informe.ErrorLog("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
		                logger.error("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
		            }
	
		        }
			}
		}else {
        	Informe.InfoLog("No existen lotes para la consulta.");
        }

        logger.info("==== ===== ======= ====");
        logger.info("NEXO SIFEN MANAGER QUIT");
        logger.info("==== ===== ======= ====");
        

    	Informe.InfoLog("==== ===== ======= ====");
    	Informe.InfoLog("NEXO SIFEN MANAGER QUIT");
    	Informe.InfoLog("==== ===== ======= ====");

    }

    public static void main(String[] args) throws IOException {

        SifenConfig sifenConfig = null;
		try {
			sifenConfig = SifenConfig.cargarConfiguracion("conf/sifen-nexo.properties");
		} catch (SifenException e) {
			// TODO Auto-generated catch block
			Informe.ErrorLog("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
		}
		if(sifenConfig != null){
        	SifenManager2 SifenManager2 = new SifenManager2(sifenConfig);
			SifenManager2.start();
		}
    }
}
