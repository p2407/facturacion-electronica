package py.com.nexo.sifen;

import com.google.gson.Gson;
import com.roshka.sifen.Sifen;
import com.roshka.sifen.core.SifenConfig;
import com.roshka.sifen.core.beans.response.RespuestaConsultaDE;
import com.roshka.sifen.core.exceptions.SifenException;
//import com.roshka.sifen.internal.util.SifenUtil;

import log.Informe;
import log.writeXMLFile;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class consultaSifenPorCdc {
    private static Connection conexion;

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(consultaSifenPorCdc.class);

    private consultaSifenPorCdc(SifenConfig sifenConfig) throws IOException {
        try {
            Sifen.setSifenConfig(sifenConfig);
        } catch (SifenException e) {
            // TODO Auto-generated catch block
            Informe.ErrorLog(
                    "Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
        }
    }

    public static boolean crearConexionOracle() {
        boolean retorno = true;
        Properties propiedades = new Properties();
        try {
            propiedades.load(new FileReader("conf/datos_usuario_DB.properties"));
        } catch (IOException ex) {
            try {
                Informe.ErrorLog("Error en la conexión de la base de datos, motivo: " + ex.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        String baseConexion = propiedades.get("baseConexion").toString();
        String usuario = propiedades.get("user").toString();
        String contrasena = propiedades.get("password").toString();

        try {
            try {
                Informe.InfoLog("Creando conexion");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conexion = DriverManager.getConnection(baseConexion, usuario, contrasena);
        } catch (SQLException | ClassNotFoundException ex) {
            try {
                Informe.ErrorLog(baseConexion + " Usuario: " + usuario + " Contrasena: " + contrasena);
                Informe.ErrorLog("Error en la conexión de la base de datos, motivo: " + ex.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        return retorno;
    }

    public static void cerrarConexion() {
        try {
            Informe.InfoLog("Cerrando conexion");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            conexion.close();
        } catch (SQLException ex) {
            try {
                Informe.ErrorLog("Error en la base de datos, motivo: " + ex.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void doCheckSifenConfig()
            throws IOException, ClassNotFoundException, NoSuchMethodException, InterruptedException {
        logger.info("Checking SIFEN CONFIG");
        final SifenConfig sifenConfig = Sifen.getSifenConfig();
        if (sifenConfig != null) {
            logger.info(sifenConfig.toString());
            try {
                Informe.InfoLog(sifenConfig.toString());

            } catch (IOException | ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
                try {
                    Informe.ErrorLog("Error, motivo: " + ex);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
                try {
                    Informe.ErrorLog("Error, motivo: " + ex);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            logger.error("consultaSifen won't work. MUST SET SIFEN CONFIG FIRST");
            try {
                Informe.ErrorLog("consultaSifen won't work. MUST SET SIFEN CONFIG FIRST");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void start() throws IOException, SifenException, SQLException {

        Informe.InfoLog("======== ==== ===== ==========");
        Informe.InfoLog("Starting NEXO SIFEN CONSULTA POR FAEL_CDC...");
        Informe.InfoLog("======== ==== ===== ==========");

        try {
            doCheckSifenConfig();
        } catch (ClassNotFoundException | NoSuchMethodException | InterruptedException ex) {
            try {
                Informe.ErrorLog("Error en la consulta de los lotes activos, motivo: " + ex);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        List<HashMap<String, String>> elements = obtenerDatosLote();
        for (int i = 0; i < elements.size(); i++) {
            HashMap<String, String> element = elements.get(i);

            String CDC = element.get("FAEL_CDC");
            int losiCodigo = Integer.parseInt(element.get("LOSI_CODIGO"));

            try {
                RespuestaConsultaDE ret = Sifen.consultaDE(CDC);
                //
                Informe.InfoLog("losiCodigo: " + losiCodigo + " CDC: " + CDC);
                Informe.InfoLog("\nRespuesta: " + new Gson().toJson(ret) + "\n");
                if (!ret.getdCodRes().isEmpty() && ret.getdCodRes().equals("0422")) { // CDC encontrado
                    String dProtAut = ret.getxContenDE().getdProtAut();
                    if (dProtAut.isEmpty() != true) {
                        // Informe.InfoLog("CDC: " + CDC + " valido para actualizar.");
                        boolean updated = actualizarLote(losiCodigo, CDC, dProtAut);
                        if (updated) {
                            Informe.InfoLog("CDC: " + CDC + " actualizado correctamente.");
                        } else {
                            Informe.ErrorLog("CDC: " + CDC + " con error al actualizar.");
                        }
                        //
                        try {
                            writeXMLFile.saveXML(
                                    ret.getRespuestaBruta(),
                                    ret.getxContenDE().getDE().getId(), "CONSULTA_CDC");
                        } catch (NullPointerException err) {
                            Informe.ErrorLog("Error al obtener el XMl del lote, motivo: " + err);
                        }
                    }
                }
            } catch (SifenException e) {
                // TODO Auto-generated catch block
                Informe.ErrorLog(
                        "\nHubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")\n");
            } catch (IOException | NullPointerException ex) {
                try {
                    Informe.ErrorLog("\nError en la consulta de los lotes activos, motivo: " + ex + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        Informe.InfoLog("==== ===== ======= ====");
        Informe.InfoLog("NEXO SIFEN CONSULTA POR FAEL_CDC QUIT");
        Informe.InfoLog("==== ===== ======= ====");

    }

    public static List<HashMap<String, String>> obtenerDatosLote() throws SQLException, IOException {
        boolean procesoExitoso = crearConexionOracle();
        if (!procesoExitoso) {
            return null;
        }
        Statement stmt = conexion.createStatement();
        List<HashMap<String, String>> elements = new ArrayList<HashMap<String, String>>();

        try {
            // First SQL SELECT Query.
            // String query_select = "select losi_codigo, fael_cdc from lote_factura_sifen
            // lofasi where fael_cdc = '01800219600001001005331022022052412409469298'";
            // String query_select = "select distinct lo.losi_codigo, lo.fael_cdc from
            // lote_factura_sifen lo, losi_grupo_msg_result msg where msg.losi_codigo =
            // lo.losi_codigo and msg.fael_cdc = lo.fael_cdc and lo.lofasi_estado <>
            // 'Reprocesado' and lo.lofasi_estado <> 'Aprobado' and
            // msg.lgmr_codigo_respuesta = '1001' and lofasi_fch_ins >= trunc(sysdate - 5)
            // union select distinct lo.losi_codigo, lo.fael_cdc from lote_factura_sifen lo,
            // losi_grupo_msg_result msg where msg.losi_codigo = lo.losi_codigo and
            // msg.fael_cdc = lo.fael_cdc and lo.lofasi_estado <> 'Reprocesado' and
            // lo.lofasi_estado <> 'Aprobado' and msg.lgmr_codigo_respuesta = '0160' and
            // msg.lgmr_msj_respuesta = 'XML malformado: DE ya encolado en otro proceso' and
            // lofasi_fch_ins >= trunc(sysdate - 5) order by losi_codigo";
            String query_select = "select distinct lo.losi_codigo, lo.fael_cdc, 1 orden\r\n" + //
                    "  from lote_factura_sifen lo, losi_grupo_msg_result msg\r\n" + //
                    " where msg.losi_codigo = lo.losi_codigo\r\n" + //
                    "   and msg.fael_cdc = lo.fael_cdc\r\n" + //
                    "   and lo.lofasi_estado <> 'Reprocesado'\r\n" + //
                    "   and lo.lofasi_estado <> 'Aprobado'\r\n" + //
                    "   and msg.lgmr_codigo_respuesta = '1001' --CDC duplicado \r\n" + //
                    "   and lofasi_fch_ins >= trunc(sysdate - 10)\r\n" + //
                    // " and lo.losi_codigo <> 27977\r\n" + //
                    "union\r\n" + //
                    "select distinct lo.losi_codigo, lo.fael_cdc, 2\r\n" + //
                    "  from lote_factura_sifen lo, losi_grupo_msg_result msg\r\n" + //
                    " where msg.losi_codigo = lo.losi_codigo\r\n" + //
                    "   and msg.fael_cdc = lo.fael_cdc\r\n" + //
                    "   and lo.lofasi_estado <> 'Reprocesado'\r\n" + //
                    "   and lo.lofasi_estado <> 'Aprobado'\r\n" + //
                    "   and msg.lgmr_codigo_respuesta = '0160'\r\n" + //
                    "   and msg.lgmr_msj_respuesta =\r\n" + //
                    "       'XML malformado: DE ya encolado en otro proceso'\r\n" + //
                    "   and lofasi_fch_ins >= trunc(sysdate - 10)\r\n" + //
                    // " and lo.losi_codigo <> 27977\r\n" + //
                    "union\r\n" + //
                    "select lofasi.losi_codigo, lofasi.fael_cdc, 3\r\n" + //
                    "  from lote_factura_sifen lofasi, lote_sifen losi, factura_electronica fael\r\n" + //
                    " where lofasi.lofasi_estado = 'AC'\r\n" + //
                    "   and losi.losi_codigo = lofasi.losi_codigo\r\n" + //
                    "   and fael.fael_cdc = lofasi.fael_cdc\r\n" + //
                    "   and losi.losi_estado = 'EN'\r\n" + //
                    "   and fael.fael_estado <> 'AP'\r\n" + //
                    "   and lofasi_fch_ins >= trunc(sysdate - 10)\r\n" + //
                    "   and fael.fael_fch_upd <= sysdate - 30 / 1440\r\n" + //
                    // " and lofasi.losi_codigo <> 27977\r\n" + //
                    " order by 3, losi_codigo\r\n" + //
                    "";
            // String query_select = "select losi_codigo, fael_cdc from lote_factura_sifen
            // lofasi where lofasi.lofasi_estado = 'Rechazado' and lofasi.losi_codigo in
            // (2978, 3059, 3049, 3051, 3056, 3055, 2980, 3052, 2977, 3057, 3050, 2942,
            // 3058, 3054, 3053, 2979) order by losi_codigo";
            // String query_select = "select losi_codigo, fael_cdc from lote_factura_sifen
            // lofasi where lofasi.lofasi_estado = 'Rechazado' and lofasi.losi_codigo in
            // (2978) order by losi_codigo";

            // Executing first SQL UPDATE query using executeUpdate() method of Statement
            // object.
            Informe.InfoLog("query_select =  " + query_select);
            // Executing SQL SELECT query using executeQuery() method of Statement object.
            ResultSet rs = stmt.executeQuery(query_select);

            // looping through the number of row/rows retrieved after executing SELECT
            // query3
            while (rs.next()) {
                // System.out.print(rs.getInt("LOSI_CODIGO") + "\t");
                // System.out.print(rs.getString("FAEL_CDC") + "\t" + "\t");
                HashMap<String, String> element = new HashMap<>();
                element.put("LOSI_CODIGO", String.valueOf(rs.getInt("LOSI_CODIGO")));
                element.put("FAEL_CDC", rs.getString("FAEL_CDC"));
                elements.add(element);
            }
            return elements;

        } catch (SQLException ex) {
            try {
                Informe.ErrorLog(
                        "Error en la base de datos al ejecutar el select, motivo: "
                                + ex.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        } finally {
            cerrarConexion();
        }

    }

    public static boolean actualizarLote(int losiCodigo, String faelCdc, String nroTransaccion)
            throws SQLException, IOException {
        boolean procesoExitoso = crearConexionOracle();
        if (!procesoExitoso) {
            return false;
        }
        Statement stmt = conexion.createStatement();

        try {
            // First SQL UPDATE Query to update record.
            String query1 = "update lote_factura_sifen set lofasi_estado='Aprobado', numero_transaccion='"
                    + nroTransaccion + "' where losi_codigo = " + losiCodigo + " and fael_cdc = '" + faelCdc
                    + "' and lofasi_estado <> 'Aprobado'";
            // Executing first SQL UPDATE query using executeUpdate() method of Statement
            // object.
            Informe.InfoLog("Query1 =  " + query1);
            int count = stmt.executeUpdate(query1);
            Informe.InfoLog("Numero de filas actualizadas ejecutando Query1 =  " + count);
            if (count > 0) {
                conexion.commit();
            } else {
                return false;
            }
        } catch (SQLException ex) {
            try {
                Informe.ErrorLog(
                        "Error en la base de datos al registrar los parametros o ejecutar el procedimiento, motivo: "
                                + ex.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        } finally {
            cerrarConexion();
        }

        return true;

    }

    public static void IniciarRemoto() throws IOException, SifenException, SQLException {

        SifenConfig sifenConfig = SifenConfig.cargarConfiguracion("conf/sifen-nexo-consulta.properties");
        consultaSifenPorCdc consultaSifen = new consultaSifenPorCdc(sifenConfig);
        consultaSifen.start();

    }

    public static void main(String[] args) throws IOException, NoSuchMethodException, ClassNotFoundException,
            InterruptedException, SifenException, SQLException {

        SifenConfig sifenConfig = SifenConfig.cargarConfiguracion("conf/sifen-nexo-consulta.properties");
        consultaSifenPorCdc consultaSifen = new consultaSifenPorCdc(sifenConfig);
        consultaSifen.start();
    }
}
