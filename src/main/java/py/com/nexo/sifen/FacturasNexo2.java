package py.com.nexo.sifen;

import com.roshka.sifen.core.beans.DocumentoElectronico;
import com.roshka.sifen.core.fields.request.de.*;
import com.roshka.sifen.core.types.*;

import querys.ParametrosFacturasNexoQuery;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FacturasNexo2 {

    public static DocumentoElectronico facturaTest01(ParametrosFacturasNexoQuery factura) {
    	
        LocalDateTime currentDate = LocalDateTime.now();

        //Conformacion del CDC
        //iTiDE(TIPO_DOCUMENTO) - dRucEm(RUC_EMISOR) - dDVEmi(DV_EMISOR) - dEst(ESTABLECIMIENTO) - dPunExp(PUNTO_EXPEDICION) - dNumDoc(NRO_DOCUMENTO) - iTipCont(TIPO_CONTRIBUYENTE) - dFeEmiDE(FECHA_EMISION) - iTipEmi(TIPO_EMISION) - dCodSeg(CODIGO_SEGURIDAD) - dDVId(DIGITO_VERIFICADOR - Obs: Generado por SIFEN con los datos anteriores)
        //Ej: 01 - 80021960 - 0 - 001 - 001 - 1021082 - 1 - 20211110 - 1 - 950832005 - 6
        
        // Grupo A
        DocumentoElectronico de = new DocumentoElectronico();
        currentDate = currentDate.truncatedTo(ChronoUnit.MINUTES);
        currentDate = currentDate.truncatedTo(ChronoUnit.SECONDS);
        de.setdFecFirma(currentDate.withNano(0));
        de.setdSisFact((short) 1);

        // Grupo B
        TgOpeDE gOpeDE = new TgOpeDE();
        gOpeDE.setiTipEmi(TTipEmi.getByVal(factura.getiTipEmi())); // PARTE DEL CDC - B002
        gOpeDE.setdCodSeg(factura.getdCodSeg()); // PARTE DEL CDC
        de.setgOpeDE(gOpeDE);

        // Grupo C
        TgTimb gTimb = new TgTimb();
        gTimb.setiTiDE(TTiDE.getByVal(factura.getiTiDE())); // PARTE DEL CDC - C002
        gTimb.setdNumTim(factura.getdNumTim()); // C004
        gTimb.setdEst(factura.getdEst()); // PARTE DEL CDC - C005
        gTimb.setdPunExp(factura.getdPunExp()); // PARTE DEL CDC - C006
        gTimb.setdNumDoc(factura.getdNumDoc()); // PARTE DEL CDC - C007
        gTimb.setdSerieNum(factura.getdSerieNum()); // C010
        gTimb.setdFeIniT(LocalDate.parse(factura.getdFeIniT()));
        de.setgTimb(gTimb);

        // Grupo D
        TdDatGralOpe dDatGralOpe = new TdDatGralOpe();
        dDatGralOpe.setdFeEmiDE(LocalDateTime.parse(factura.getdFeEmiDE())); // PARTE DEL CDC

        TgOpeCom gOpeCom = new TgOpeCom();
        
        if (de.getgTimb().getiTiDE().getVal() == 1 || de.getgTimb().getiTiDE().getVal() == 4)
        	gOpeCom.setiTipTra(TTipTra.getByVal(factura.getiTipTra()));
        
        gOpeCom.setiTImp(TTImp.IVA);
        gOpeCom.setcMoneOpe(factura.getcMoneTiPag());
        
        if (factura.getcMoneTiPag() != CMondT.PYG){
        	gOpeCom.setdCondTiCam(TdCondTiCam.GLOBAL);
        	gOpeCom.setdTiCam(factura.getdTiCam());
        }
        	
        dDatGralOpe.setgOpeCom(gOpeCom);

        TgEmis gEmis = new TgEmis();
        gEmis.setdRucEm(factura.getdRucEm()); // PARTE DEL CDC
        gEmis.setdDVEmi(factura.getdDVEmi()); // PARTE DEL CDC
        gEmis.setiTipCont(TiTipCont.getByVal(factura.getiTipCont())); // PARTE DEL CDC
        gEmis.setdNomEmi(factura.getdNomEmi());
        gEmis.setdDirEmi(factura.getdDirEmi());
        gEmis.setdNumCas(factura.getdNumCas());
        gEmis.setcDepEmi(TDepartamento.CAPITAL);
        gEmis.setcCiuEmi(1);
        gEmis.setdDesCiuEmi(factura.getdDesCiuEmi());
        gEmis.setdTelEmi(factura.getdTelEmi());
        gEmis.setdEmailE(factura.getdEmailE());

        List<TgActEco> gActEcoList = new ArrayList<>();
        for (int i = 0; i < factura.getActividadesEconomicasNexoQuery().size(); i++) {
            TgActEco gActEco = new TgActEco();
            gActEco.setcActEco(factura.getActividadesEconomicasNexoQuery().get(i).getcActEco());
            gActEco.setdDesActEco(factura.getActividadesEconomicasNexoQuery().get(i).getdDesActEco());
            gActEcoList.add(gActEco);
        }

        gEmis.setgActEcoList(gActEcoList);
        dDatGralOpe.setgEmis(gEmis);

        TgDatRec gDatRec = new TgDatRec();
        if (factura.getiNatRec() == 1) {
            gDatRec.setiNatRec(TiNatRec.CONTRIBUYENTE);
            gDatRec.setiTiContRec(TiTipCont.getByVal(factura.getiTiContRec()));
            gDatRec.setdRucRec(factura.getdRucRec());
            gDatRec.setdDVRec(factura.getdDVRec());
        } else if (factura.getiNatRec() == 2) {
            gDatRec.setiNatRec(TiNatRec.NO_CONTRIBUYENTE);
            gDatRec.setdNumIDRec(factura.getdNumIDRec());
        }
        //gDatRec.setdEmailRec(factura.getdEmailRec()); Se comento porque son datos no mantenidos en el sistema y al rechazarse generan un retraso en los desarrollos.
        //gDatRec.setdCelRec(factura.getdCelRec()); Se comento porque son datos no mantenidos en el sistema y al rechazarse generan un retraso en los desarrollos.
        gDatRec.setiTiOpe(TiTiOpe.B2C);
        gDatRec.setcPaisRec(PaisType.PRY);
        gDatRec.setiTipIDRec(TiTipDocRec.CEDULA_PARAGUAYA);
        gDatRec.setdNomRec(factura.getdNomRec());
        dDatGralOpe.setgDatRec(gDatRec);
        de.setgDatGralOpe(dDatGralOpe);

        // Grupo E
        TgDtipDE gDtipDE = new TgDtipDE();

        // Grupo E1
        TgCamFE gCamFE = new TgCamFE();
        gCamFE.setiIndPres(TiIndPres.OPERACION_ELECTRONICA);
        gDtipDE.setgCamFE(gCamFE);

        // Grupo E7
        TgCamCond gCamCond = new TgCamCond();
        gCamCond.setiCondOpe(TiCondOpe.getByVal(factura.getiCondOpe()));
        
        List<TgPaConEIni> pagos = new ArrayList<>();
        
        for (int i = 0; i < factura.getDetalleFacturaQuery().getFormaPago().size(); i++) {
            TgPaConEIni pago = new TgPaConEIni();
            pago.setcMoneTiPag(factura.getDetalleFacturaQuery().getFormaPago().get(i).getMoneCodigo());
            if(pago.getcMoneTiPag() != CMondT.PYG){
                pago.setdTiCamTiPag(factura.getdTiCam());
            }
            pago.setiTiPago(TiTiPago.getByVal(factura.getDetalleFacturaQuery().getFormaPago().get(i).getFormaPago()));
            pago.setdMonTiPag(factura.getDetalleFacturaQuery().getFormaPago().get(i).getMonto());
            if (pago.getiTiPago().getVal() == 2){
                TgPagCheq gPagCheq = new TgPagCheq();
                gPagCheq.setdNumCheq(factura.getDetalleFacturaQuery().getFormaPago().get(i).getdNumCheq());
                gPagCheq.setdBcoEmi(factura.getDetalleFacturaQuery().getFormaPago().get(i).getdBcoEmi()); //Ver que pasarle
                pago.setgPagCheq(gPagCheq);
            }
            if (pago.getiTiPago().getVal() == 3 || pago.getiTiPago().getVal() == 4){
                TgPagTarCD gPagTarCD = new TgPagTarCD();

                pago.setgPagTarCD(gPagTarCD);
            }
            if (pago.getiTiPago().getVal() == 99){
                pago.setdDesTiPag(factura.getDetalleFacturaQuery().getFormaPago().get(i).getFormaPagoDesc());
            }
            pagos.add(pago);
        }
        
        if(gCamCond.getiCondOpe().getVal() == 1){
        	gCamCond.setgPaConEIniList(pagos); // Obligatorio si iCondOpe = 1 ó Obligatorio si existe el campo dMonEnt
        }

        gDtipDE.setgCamCond(gCamCond);

        List<TgCamItem> gCamItemList = new ArrayList<>();
        for (int i = 0; i < factura.getDetalleFacturaQuery().getDetalle().size(); i++) {
            TgCamItem gCamItem = new TgCamItem();
            gCamItem.setdCodInt(factura.getDetalleFacturaQuery().getDetalle().get(i).getdCodInt());
            gCamItem.setdDesProSer(factura.getDetalleFacturaQuery().getDetalle().get(i).getdDesProSer());
            gCamItem.setcUniMed(TcUniMed.UNI);
            gCamItem.setdCantProSer(BigDecimal.valueOf(1));

            TgValorItem gValorItem = new TgValorItem();
            gValorItem.setdPUniProSer(factura.getDetalleFacturaQuery().getDetalle().get(i).getdPUniProSer());
            
            TgValorRestaItem gValorRestaItem = new TgValorRestaItem();
            gValorItem.setgValorRestaItem(gValorRestaItem);
            gCamItem.setgValorItem(gValorItem);

            TgCamIVA gCamIVA = new TgCamIVA();
            gCamIVA.setiAfecIVA(factura.getDetalleFacturaQuery().getDetalle().get(i).getiAfecIVA());
            gCamIVA.setdPropIVA(factura.getDetalleFacturaQuery().getDetalle().get(i).getdPropIVA());
            gCamIVA.setdTasaIVA(factura.getDetalleFacturaQuery().getDetalle().get(i).getdTasaIVA());
            gCamItem.setgCamIVA(gCamIVA);

            gCamItemList.add(gCamItem);
        }

        gDtipDE.setgCamItemList(gCamItemList);
        de.setgDtipDE(gDtipDE);
        
        // Grupo E7.2
        //Hablado con Tesoreria - Si la factura es a credito entonces la condicion de credito seria siempre a cuotas y la cantidad de cuotas es 1
        TgCuotas gCuotas = new TgCuotas();
        List<TgCuotas> gCuotasList = new ArrayList<>();
        TgPagCred gPagCred = new TgPagCred();
        if(gCamCond.getiCondOpe().getVal() == 2){ //Factura a Credito
            LocalDate dVencCuo = LocalDate.parse(factura.getdVencCuo());
            //
            gCuotas.setcMoneCuo(factura.getcMoneTiPag());
            gCuotas.setdMonCuota(factura.getdMonCuota());
            gCuotas.setdVencCuo(dVencCuo);
            //
            gCuotasList.add(gCuotas);
            gPagCred.setiCondCred(TiCondCred.CUOTA);
            gPagCred.setdCuotas((short) 1);
            gPagCred.setgCuotasList(gCuotasList);
	        gCamCond.setgPagCred(gPagCred);
        }

        // Grupo E
        de.setgTotSub(new TgTotSub());
        
        Collection<Short> listaParaNotaCredito = new ArrayList<Short>(); //Obligatorio si iTiDE esta en esta lista
        listaParaNotaCredito.add((short) 5); //5(Nota de crédito electrónica)
        listaParaNotaCredito.add((short) 6); //6(Nota de débito electrónica)
        
        // Grupo E5 -> Obligatorio si iTiDE = 5(Nota de crédito electrónica) o 6(Nota de débito electrónica) (NCE y NDE), No informar si iTiDE ≠ 5(Nota de crédito electrónica) o 6(Nota de débito electrónica)
        TgCamNCDE gCamNCDE = new TgCamNCDE();
        gCamNCDE.setiMotEmi(TiMotEmi.getByVal(factura.getiMotEmi())); //Parametro fijo
        if (listaParaNotaCredito.contains(de.getgTimb().getiTiDE().getVal())){
            de.getgDtipDE().setgCamNCDE(gCamNCDE);
        }
        
        // Grupo H -> 
        ///(Obligatorio si iTiDE = 4(Autofactura electrónica), 5(Nota de crédito electrónica), 6(Nota de débito electrónica))
        //(Opcional si iTiDE = 1(Factura electrónica) o 7(Nota de remisión electrónica))

        List<TgCamDEAsoc> gCamDEAsocList = null;
        TgCamDEAsoc gCamDEAsoc = null;
        if (listaParaNotaCredito.contains(de.getgTimb().getiTiDE().getVal())){
            gCamDEAsocList = new ArrayList<TgCamDEAsoc>();
            gCamDEAsoc = new TgCamDEAsoc();
            gCamDEAsoc.setiTipDocAso(TiTipDocAso.getByVal(factura.getiTipDocAso())); //Tipo de documento asociado -> 1= Electrónico - 2= Impreso - 3= Constancia Electrónica

            if (gCamDEAsoc.getiTipDocAso() == TiTipDocAso.getByVal((short) 1)) {//Obligatorio si iTipDocAso = 1, No informar si iTipDocAso = 2 o 3
                gCamDEAsoc.setdCdCDERef(factura.getdCdCDERef()); //CDC del DTE referenciado
            }
            if (gCamDEAsoc.getiTipDocAso() == TiTipDocAso.getByVal((short) 2)){ //Obligatorio si iTipDocAso = 2, No informar si iTipDocAso = 1 o 3
                gCamDEAsoc.setdNTimDI(factura.getdNTimDI()); //Nro. timbrado documento impreso de referencia
                gCamDEAsoc.setdEstDocAso(factura.getdEstDocAso()); //Establecimiento -> Completar con 0 (cero) a la izquierda
                gCamDEAsoc.setdPExpDocAso(factura.getdPExpDocAso()); //Punto de expedición -> Completar con 0 (cero) a la izquierda
                gCamDEAsoc.setdNumDocAso(factura.getdNumDocAso()); //Número del documento -> Completar con 0 (cero) a la izquierda hasta alcanzar 7 (siete) cifras
                gCamDEAsoc.setiTipoDocAso(TiTIpoDoc.getByVal(factura.getiTipoDocAso())); //Tipo de documento impreso -> 1= Factura, 2= Nota de crédito, 3= Nota de débito, 4= Nota de remisión, 5= Comprobante de retención
                gCamDEAsoc.setdFecEmiDI(LocalDate.parse(factura.getdFecEmiDI())); //Fecha de emisión del documento impreso de referencia -> Formato AAAA-MM-DD,  Obligatorio solo si existe el campo dNTimDI
            }
            listaParaNotaCredito.add((short) 4); //4(Autofactura electrónica)
            if (listaParaNotaCredito.contains(de.getgTimb().getiTiDE().getVal())){
                gCamDEAsocList.add(gCamDEAsoc);
                de.setgCamDEAsocList(gCamDEAsocList);
            }
        }

        return de;

    }
    
}
