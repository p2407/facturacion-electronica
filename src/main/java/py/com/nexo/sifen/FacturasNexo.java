package py.com.nexo.sifen;

import com.roshka.sifen.core.beans.DocumentoElectronico;
import com.roshka.sifen.core.fields.request.de.*;
import com.roshka.sifen.core.types.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FacturasNexo {

    public static DocumentoElectronico facturaTest01() {
        LocalDateTime currentDate = LocalDateTime.now();

        // Grupo A
        DocumentoElectronico de = new DocumentoElectronico();
        de.setdFecFirma(currentDate);
        de.setdSisFact((short) 1);

        // Grupo B
        TgOpeDE gOpeDE = new TgOpeDE();
        gOpeDE.setiTipEmi(TTipEmi.NORMAL);
        de.setgOpeDE(gOpeDE);

        // Grupo C
        TgTimb gTimb = new TgTimb();
        gTimb.setiTiDE(TTiDE.FACTURA_ELECTRONICA);
        gTimb.setdNumTim(12557662);
        gTimb.setdEst("001");
        gTimb.setdPunExp("002");
        gTimb.setdNumDoc("0000007");
        gTimb.setdFeIniT(LocalDate.parse("2019-07-31"));
        de.setgTimb(gTimb);

        // Grupo D
        TdDatGralOpe dDatGralOpe = new TdDatGralOpe();
        dDatGralOpe.setdFeEmiDE(currentDate);

        TgOpeCom gOpeCom = new TgOpeCom();
        gOpeCom.setiTipTra(TTipTra.PRESTACION_SERVICIOS);
        gOpeCom.setiTImp(TTImp.IVA);
        gOpeCom.setcMoneOpe(CMondT.PYG);
        dDatGralOpe.setgOpeCom(gOpeCom);

        TgEmis gEmis = new TgEmis();
        gEmis.setdRucEm("80021960");
        gEmis.setdDVEmi("0");
        gEmis.setiTipCont(TiTipCont.PERSONA_JURIDICA);
        gEmis.setdNomEmi("de generado en ambiente de prueba - sin valor comercial ni fiscal");
        gEmis.setdDirEmi("R.I.4 Curupayty");
        gEmis.setdNumCas("264");
        gEmis.setcDepEmi(TDepartamento.CAPITAL);
        gEmis.setcCiuEmi(1);
        gEmis.setdDesCiuEmi("ASUNCION (DISTRITO)");
        gEmis.setdTelEmi("0216193000");
        gEmis.setdEmailE("administracion@nexo.com.py");

        List<TgActEco> gActEcoList = new ArrayList<>();
        TgActEco gActEco = new TgActEco();
        gActEco.setcActEco("66100");
        gActEco.setdDesActEco("ACTIVIDADES AUXILIARES DE LA INTERMEDIACIÓN FINANCIERA, EXCEPTO LA FINANCIACIÓN DE PLANES DE SEGUROS Y DE PENSIONES");
        gActEcoList.add(gActEco);

        TgActEco gActEco2 = new TgActEco();
        gActEco2.setcActEco("63110");
        gActEco2.setdDesActEco("PROCESAMIENTO DE DATOS, HOSPEDAJE Y SERVICIOS CONEXOS");
        gActEcoList.add(gActEco2);

        gEmis.setgActEcoList(gActEcoList);
        dDatGralOpe.setgEmis(gEmis);

        TgDatRec gDatRec = new TgDatRec();
        gDatRec.setiNatRec(TiNatRec.NO_CONTRIBUYENTE);
        gDatRec.setiTiOpe(TiTiOpe.B2C);
        gDatRec.setcPaisRec(PaisType.PRY);
        gDatRec.setiTipIDRec(TiTipDocRec.CEDULA_PARAGUAYA);
        gDatRec.setdNumIDRec("4579993");
        gDatRec.setdNomRec("Martin Zarza");
        dDatGralOpe.setgDatRec(gDatRec);
        de.setgDatGralOpe(dDatGralOpe);

        // Grupo E
        TgDtipDE gDtipDE = new TgDtipDE();

        TgCamFE gCamFE = new TgCamFE();
        gCamFE.setiIndPres(TiIndPres.OPERACION_ELECTRONICA);
        gDtipDE.setgCamFE(gCamFE);

        TgCamCond gCamCond = new TgCamCond();
        gCamCond.setiCondOpe(TiCondOpe.CONTADO);

        List<TgPaConEIni> pagos = new ArrayList<>();
        TgPaConEIni pago = new TgPaConEIni();
        pagos.add(pago);
        pago.setcMoneTiPag(CMondT.PYG);
        pago.setdTiCamTiPag(BigDecimal.ONE);

        gCamCond.setgPaConEIniList(new ArrayList<>());


        gDtipDE.setgCamCond(gCamCond);


        List<TgCamItem> gCamItemList = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            TgCamItem gCamItem = new TgCamItem();
            gCamItem.setdCodInt(i == 0 ? "001" : "002");
            gCamItem.setdDesProSer(i == 0 ? "MORATORIO" : "CAPITAL");
            gCamItem.setcUniMed(TcUniMed.UNI);
            gCamItem.setdCantProSer(BigDecimal.valueOf(1));

            TgValorItem gValorItem = new TgValorItem();
            gValorItem.setdPUniProSer(BigDecimal.valueOf(120000));

            TgValorRestaItem gValorRestaItem = new TgValorRestaItem();
            gValorItem.setgValorRestaItem(gValorRestaItem);
            gCamItem.setgValorItem(gValorItem);

            if (i == 0) {
                TgCamIVA gCamIVA = new TgCamIVA();
                gCamIVA.setiAfecIVA(TiAfecIVA.GRAVADO);
                gCamIVA.setdPropIVA(BigDecimal.valueOf(100));
                gCamIVA.setdTasaIVA(BigDecimal.valueOf(5));
                gCamItem.setgCamIVA(gCamIVA);
            } else {
                TgCamIVA gCamIVA = new TgCamIVA();
                gCamIVA.setiAfecIVA(TiAfecIVA.EXENTO);
                gCamIVA.setdPropIVA(BigDecimal.valueOf(100));
                gCamIVA.setdTasaIVA(BigDecimal.ZERO);
                gCamItem.setgCamIVA(gCamIVA);
            }

            gCamItemList.add(gCamItem);
        }

        gDtipDE.setgCamItemList(gCamItemList);
        de.setgDtipDE(gDtipDE);

        // Grupo E
        de.setgTotSub(new TgTotSub());

        return de;

    }



}
