package py.com.nexo.sifen;

import com.google.gson.Gson;
import com.roshka.sifen.Sifen;
import com.roshka.sifen.core.SifenConfig;
import com.roshka.sifen.core.beans.DocumentoElectronico;
import com.roshka.sifen.core.beans.response.RespuestaRecepcionDE;
import com.roshka.sifen.core.beans.response.RespuestaRecepcionLoteDE;
import com.roshka.sifen.core.exceptions.SifenException;

import conexion.ObtenerDatosFacturasOracle;
import log.Informe;
import querys.ActividadesEconomicasNexoQuery;
import querys.LotesSifenQuery;
import querys.ParametrosFacturasNexoQuery;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class envioSifenUnitario {
    
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(envioSifenUnitario.class);

    private envioSifenUnitario(SifenConfig sifenConfig) throws IOException {
        try {
			Sifen.setSifenConfig(sifenConfig);
		} catch (SifenException e) {
			// TODO Auto-generated catch block
			Informe.ErrorLog("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
		}
    }
    
    private void doCheckSifenConfig() throws IOException {
        logger.info("Checking SIFEN CONFIG");
        final SifenConfig sifenConfig = Sifen.getSifenConfig();
        if (sifenConfig != null) {
            logger.info(sifenConfig.toString());
	    	try {
				Informe.InfoLog(sifenConfig.toString());
	        } catch (IOException | ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
	        } catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
	        }
        } else {
            logger.error("envioSifen won't work. MUST SET SIFEN CONFIG FIRST");
	    	try {
				Informe.ErrorLog("envioSifen won't work. MUST SET SIFEN CONFIG FIRST");
			} catch (IOException e) {
				e.printStackTrace();
			}
        }

    }

    private void start() throws IOException, ClassNotFoundException, NoSuchMethodException, InterruptedException {
        
    	Informe.InfoLog("======== ==== ===== ==========");
    	Informe.InfoLog("Starting NEXO SIFEN ENVIO...");
    	Informe.InfoLog("======== ==== ===== ==========");

        doCheckSifenConfig();

        List<LotesSifenQuery> lotesSifenActivos = null;
        
        try {
			lotesSifenActivos = ObtenerDatosFacturasOracle.cargaDatosLotesSifen();
		} catch (SQLException ex) {
			try {
				Informe.ErrorLog("Error en la consulta de los lotes activos, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
        } catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
			try {
				Informe.ErrorLog("Error, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
        } catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
			try {
				Informe.ErrorLog("Error, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
        
        if (lotesSifenActivos != null && lotesSifenActivos.size() > 0) {
        	//Se le llama una vez cada que corre el programa, para no sobrecargar debalde la base
        	List<ActividadesEconomicasNexoQuery> ActividadesEconomicas = ObtenerDatosFacturasOracle.cargaDatosActividadesEconomicasNexo();
        	
        	for (int lsa = 0; lsa < lotesSifenActivos.size(); lsa++) {
        	//for (int lsa = 0; lsa < 1; lsa++) { //Enviar un solo lote por vez
		        try {
		            List<DocumentoElectronico> lote = new ArrayList<>();
					List<ParametrosFacturasNexoQuery> facturas = null;
					try {
						facturas = ObtenerDatosFacturasOracle.cargaDatosFacturacionElectronica(lotesSifenActivos.get(lsa).getLosiCodigo());
					} catch (SQLException ex) {
						try {
							Informe.ErrorLog("Error, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
			        } catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
						try {
							Informe.ErrorLog("Error, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
			        } catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
						try {
							Informe.ErrorLog("Error, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
			        }
					//Loop del lote de facturas
					try {
						for (int i = 0; i < facturas.size(); i++) {
							facturas.get(i).setActividadesEconomicasNexoQuery(ActividadesEconomicas);
						    lote.add(FacturasNexo2.facturaTest01(facturas.get(i)));
							break;
						}
			        } catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
						try {
							Informe.ErrorLog("Error, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
			        } catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
						try {
							Informe.ErrorLog("Error, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
			        }
		            catch (Throwable ex){
		                //Capturando la excepción (superclase)
						try {
							Informe.ErrorLog("Error, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
			        }
		
					Informe.InfoLog("Datos del Lote en crudo: " + new Gson().toJson(facturas));
					Informe.InfoLog("Datos del Lote formateado: " + new Gson().toJson(lote));
			    	//final RespuestaRecepcionLoteDE respuestaRecepcionLoteDE = Sifen.recepcionLoteDE(lote);
					for (int lot = 0; lot < lote.size(); lot++) {
                        RespuestaRecepcionDE ret = Sifen.recepcionDE(lote.get(lot));
						Informe.InfoLog("XML del CDC: " + lote.get(lot).obtenerCDC() + "\n" + lote.get(lot).generarXml(null) + "\n");
                        Informe.InfoLog("Respuesta del CDC: " + lote.get(lot).obtenerCDC() + "\n" + ret.toString());
                        Informe.InfoLog("Respuesta: " + new Gson().toJson(ret));
                        break;
					}
			    	/* Informe.InfoLog("Respuesta: " + new Gson().toJson(respuestaRecepcionLoteDE));
		            logger.info("Código de respuesta: " + respuestaRecepcionLoteDE.getdCodRes() + " - Fecha: " + respuestaRecepcionLoteDE.getdFecProc());
	                
	                Informe.InfoLog("Codigo de Lote: " + respuestaRecepcionLoteDE.getdProtConsLote() + " Codigo de Respuesta: *" + respuestaRecepcionLoteDE.getdCodRes() + "* Mensaje de Respuesta: " + respuestaRecepcionLoteDE.getdMsgRes());
			    	
			    	Informe.InfoLog("RAW ANSWER " + respuestaRecepcionLoteDE.getRespuestaBruta());
		            logger.info("RAW ANSWER " + respuestaRecepcionLoteDE.getRespuestaBruta());
		        	
		        	lotesSifenActivos.get(lsa).setdProtConsLote(respuestaRecepcionLoteDE.getdProtConsLote());
		        	lotesSifenActivos.get(lsa).setdCodRes(respuestaRecepcionLoteDE.getdCodRes());
		        	lotesSifenActivos.get(lsa).setdMsgRes(respuestaRecepcionLoteDE.getdMsgRes()); */
		
		        } catch (SifenException e) {
			    	Informe.ErrorLog("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
		            logger.error("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
		        } catch (IOException | ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
					try {
						Informe.ErrorLog("Error, motivo: " + ex);
					} catch (IOException e) {
						e.printStackTrace();
					}
		        } catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
					try {
						Informe.ErrorLog("Error, motivo: " + ex);
					} catch (IOException e) {
						e.printStackTrace();
					}
		        }
                break;
	        }
        }else {
        	Informe.InfoLog("No existen lotes activos.");
        }

    	Informe.InfoLog("==== ===== ======= ====");
    	Informe.InfoLog("NEXO SIFEN ENVIO QUIT");
    	Informe.InfoLog("==== ===== ======= ====");

    }

    public static void main(String[] args) throws IOException {

        SifenConfig sifenConfig;
        try {
			sifenConfig = SifenConfig.cargarConfiguracion("conf/sifen-nexo.properties");
        	envioSifenUnitario envioSifen = new envioSifenUnitario(sifenConfig);
			envioSifen.start();
		} catch (ClassNotFoundException | NoSuchMethodException | InterruptedException ex) {
			try {
				Informe.ErrorLog("Error, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (SifenException e1) {
			// TODO Auto-generated catch block
			Informe.ErrorLog("Hubo un error en la librería SIFEN. Código: " + e1.getCode() + " (" + e1.getMessage() + ")");
			e1.printStackTrace();
		}
    }
}
