package py.com.nexo.sifen;

import com.google.gson.Gson;
import com.roshka.sifen.Sifen;
import com.roshka.sifen.core.SifenConfig;
import com.roshka.sifen.core.beans.response.RespuestaConsultaLoteDE;
import com.roshka.sifen.core.exceptions.SifenException;
//import com.roshka.sifen.internal.util.SifenUtil;

import conexion.ObtenerDatosFacturasOracle;
import log.Informe;
import log.writeXMLFile;
import querys.ConsultaLotesSifenEntregadosQuery;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class consultaSifen {

	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(consultaSifen.class);

	private consultaSifen(SifenConfig sifenConfig) throws IOException {
		try {
			Sifen.setSifenConfig(sifenConfig);
		} catch (SifenException e) {
			// TODO Auto-generated catch block
			Informe.ErrorLog(
					"Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
		}
	}

	private void doCheckSifenConfig()
			throws IOException, ClassNotFoundException, NoSuchMethodException, InterruptedException {
		logger.info("Checking SIFEN CONFIG");
		final SifenConfig sifenConfig = Sifen.getSifenConfig();
		if (sifenConfig != null) {
			logger.info(sifenConfig.toString());
			try {
				Informe.InfoLog(sifenConfig.toString());

			} catch (IOException | ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			logger.error("consultaSifen won't work. MUST SET SIFEN CONFIG FIRST");
			try {
				Informe.ErrorLog("consultaSifen won't work. MUST SET SIFEN CONFIG FIRST");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private void start() throws IOException {

		Informe.InfoLog("======== ==== ===== ==========");
		Informe.InfoLog("Starting NEXO SIFEN CONSULTA...");
		Informe.InfoLog("======== ==== ===== ==========");

		try {
			doCheckSifenConfig();
		} catch (ClassNotFoundException | NoSuchMethodException | InterruptedException ex) {
			try {
				Informe.ErrorLog("Error en la consulta de los lotes activos, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		String dProtConsLote;

		List<ConsultaLotesSifenEntregadosQuery> dProtConsLoteDetalle = null;
		try {
			dProtConsLoteDetalle = ObtenerDatosFacturasOracle.consultaLotesSifenEntregados();
		} catch (SQLException ex) {
			try {
				Informe.ErrorLog("Error en la consulta de los lotes activos, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
			try {
				Informe.ErrorLog("Error, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
			try {
				Informe.ErrorLog("Error, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (dProtConsLoteDetalle != null && dProtConsLoteDetalle.size() > 0) {
			for (int i = 0; i < dProtConsLoteDetalle.size(); i++) {
				dProtConsLote = null;
				dProtConsLote = dProtConsLoteDetalle.get(i).getdProtConsLote();

				if (dProtConsLote != null) {
					logger.info("Consultando LOTE - " + dProtConsLote);
					Informe.InfoLog("Consultando LOTE - " + dProtConsLote);

					try {
						final RespuestaConsultaLoteDE respuestaConsultaLoteDE = Sifen.consultaLoteDE(dProtConsLote);
						Informe.InfoLog("RespuestaBruta: \n" + respuestaConsultaLoteDE.getRespuestaBruta() + "\n");
						Informe.InfoLog("Respuesta: " + respuestaConsultaLoteDE.getdCodRes());
						Informe.InfoLog("Estado de la consulta: " + respuestaConsultaLoteDE.getCodigoEstado()
								+ " Codigo de Respuesta: *" + respuestaConsultaLoteDE.getdCodResLot()
								+ "* Descripcion: " + respuestaConsultaLoteDE.getdMsgResLot());
						//
						try {
							writeXMLFile.saveXML(
									respuestaConsultaLoteDE.getRespuestaBruta(),
									dProtConsLote, "CONSULTA_LOTES");
						} catch (NullPointerException err) {
							Informe.ErrorLog("Error al obtener el XMl del lote, motivo: " + err);
						}

						String dCodResLot = respuestaConsultaLoteDE.getdCodResLot();

						if (dCodResLot != null && dCodResLot.equals("0362")) { // (Procesamiento de lote concluido)
							Informe.InfoLog(new Gson().toJson(respuestaConsultaLoteDE.getgResProcLoteList()));
							try {
								ObtenerDatosFacturasOracle.actualizaLotesEntregadosSifen(dProtConsLote,
										respuestaConsultaLoteDE.getgResProcLoteList());
							} catch (SQLException ex) {
								try {
									Informe.ErrorLog("Error en la actualizacion de los datos del lotes: "
											+ dProtConsLote + ", motivo: " + ex);
								} catch (IOException e) {
									e.printStackTrace();
								}
							} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
								try {
									Informe.ErrorLog("Error, motivo: " + ex);
								} catch (IOException e) {
									e.printStackTrace();
								}
							} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError
									| StackOverflowError ex) {
								try {
									Informe.ErrorLog("Error, motivo: " + ex);
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						}
						if (dCodResLot == null) {
							Informe.ErrorLog("********** POSIBLE ERROR EN EL SISTEMA DE LA SET **********");
						}
					} catch (SifenException e) {
						Informe.ErrorLog("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " ("
								+ e.getMessage() + ")");
						logger.error("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " ("
								+ e.getMessage() + ")");

					} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
						try {
							Informe.ErrorLog("Error, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
					} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError
							| StackOverflowError ex) {
						try {
							Informe.ErrorLog("Error, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		} else {
			Informe.InfoLog("No existen lotes para la consulta.");
		}

		Informe.InfoLog("==== ===== ======= ====");
		Informe.InfoLog("NEXO SIFEN CONSULTA QUIT");
		Informe.InfoLog("==== ===== ======= ====");

	}

	private void startUnoAUno() throws IOException {

		Informe.InfoLog("======== ==== ===== ==========");
		Informe.InfoLog("Starting NEXO SIFEN CONSULTA...");
		Informe.InfoLog("======== ==== ===== ==========");

		try {
			doCheckSifenConfig();
		} catch (ClassNotFoundException | NoSuchMethodException | InterruptedException ex) {
			try {
				Informe.ErrorLog("Error en la consulta de los lotes activos, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		String dProtConsLote = "11455905516374106";

		if (dProtConsLote != null) {
			logger.info("Consultando LOTE - " + dProtConsLote);
			Informe.InfoLog("Consultando LOTE - " + dProtConsLote);

			try {
				final RespuestaConsultaLoteDE respuestaConsultaLoteDE = Sifen.consultaLoteDE(dProtConsLote);
				Informe.InfoLog("RespuestaBruta: \n" + respuestaConsultaLoteDE.getRespuestaBruta() + "\n");
				Informe.InfoLog("Respuesta: " + respuestaConsultaLoteDE.getdCodRes());
				Informe.InfoLog("Estado de la consulta: " + respuestaConsultaLoteDE.getCodigoEstado()
						+ " Codigo de Respuesta: *" + respuestaConsultaLoteDE.getdCodResLot() + "* Descripcion: "
						+ respuestaConsultaLoteDE.getdMsgResLot());
				//
				try {
					writeXMLFile.saveXML(
							respuestaConsultaLoteDE.getRespuestaBruta(),
							dProtConsLote, "CONSULTA_LOTES");
				} catch (NullPointerException err) {
					Informe.ErrorLog("Error al obtener el XMl del lote, motivo: " + err);
				}

				String dCodResLot = respuestaConsultaLoteDE.getdCodResLot();

				if (dCodResLot != null && dCodResLot.equals("0362") && false) { // (Procesamiento de lote concluido)
					Informe.InfoLog(new Gson().toJson(respuestaConsultaLoteDE.getgResProcLoteList()));
					try {
						ObtenerDatosFacturasOracle.actualizaLotesEntregadosSifen(dProtConsLote,
								respuestaConsultaLoteDE.getgResProcLoteList());
					} catch (SQLException ex) {
						try {
							Informe.ErrorLog("Error en la actualizacion de los datos del lotes: " + dProtConsLote
									+ ", motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
					} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
						try {
							Informe.ErrorLog("Error, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
					} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError
							| StackOverflowError ex) {
						try {
							Informe.ErrorLog("Error, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				if (dCodResLot == null) {
					Informe.ErrorLog("********** POSIBLE ERROR EN EL SISTEMA DE LA SET **********");
				}
			} catch (SifenException e) {
				Informe.ErrorLog(
						"Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
				logger.error(
						"Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");

			} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		Informe.InfoLog("==== ===== ======= ====");
		Informe.InfoLog("NEXO SIFEN CONSULTA QUIT");
		Informe.InfoLog("==== ===== ======= ====");

	}

	public static void main(String[] args)
			throws IOException, NoSuchMethodException, ClassNotFoundException, InterruptedException {

		try {
			SifenConfig sifenConfig = SifenConfig.cargarConfiguracion("conf/sifen-nexo.properties");
			consultaSifen consultaSifen = new consultaSifen(sifenConfig);
			consultaSifen.start();
		} catch (SifenException e) {
			// TODO Auto-generated catch block
			Informe.ErrorLog(
					"Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
		}
		// consultaSifen.startUnoAUno();
	}
}
