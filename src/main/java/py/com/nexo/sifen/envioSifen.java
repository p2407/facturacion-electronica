package py.com.nexo.sifen;

import com.google.gson.Gson;
import com.roshka.sifen.Sifen;
import com.roshka.sifen.core.SifenConfig;
import com.roshka.sifen.core.beans.DocumentoElectronico;
import com.roshka.sifen.core.beans.response.RespuestaRecepcionLoteDE;
import com.roshka.sifen.core.exceptions.SifenException;
import com.roshka.sifen.internal.ctx.GenerationCtx;

import conexion.ObtenerDatosFacturasOracle;
import log.Informe;
import log.writeXMLFile;
import querys.ActividadesEconomicasNexoQuery;
import querys.LotesSifenQuery;
import querys.ParametrosFacturasNexoQuery;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class envioSifen {

	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(envioSifen.class);

	private envioSifen(SifenConfig sifenConfig) throws IOException {
		try {
			Sifen.setSifenConfig(sifenConfig);
		} catch (SifenException e) {
			// TODO Auto-generated catch block
			Informe.ErrorLog(
					"Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
		}
	}

	private void doCheckSifenConfig() throws IOException {
		logger.info("Checking SIFEN CONFIG");
		final SifenConfig sifenConfig = Sifen.getSifenConfig();
		if (sifenConfig != null) {
			logger.info(sifenConfig.toString());
			try {
				Informe.InfoLog(sifenConfig.toString());
			} catch (IOException | ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			logger.error("envioSifen won't work. MUST SET SIFEN CONFIG FIRST");
			try {
				Informe.ErrorLog("envioSifen won't work. MUST SET SIFEN CONFIG FIRST");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private void start() throws IOException, ClassNotFoundException, NoSuchMethodException, InterruptedException {

		Informe.InfoLog("======== ==== ===== ==========");
		Informe.InfoLog("Starting NEXO SIFEN ENVIO...");
		Informe.InfoLog("======== ==== ===== ==========");

		doCheckSifenConfig();

		List<LotesSifenQuery> lotesSifenActivos = null;

		try {
			lotesSifenActivos = ObtenerDatosFacturasOracle.cargaDatosLotesSifen();
		} catch (SQLException ex) {
			try {
				Informe.ErrorLog("Error en la consulta de los lotes activos, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
			try {
				Informe.ErrorLog("Error, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
			try {
				Informe.ErrorLog("Error, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (lotesSifenActivos != null && lotesSifenActivos.size() > 0) {
			// Se le llama una vez cada que corre el programa, para no sobrecargar debalde
			// la base
			List<ActividadesEconomicasNexoQuery> ActividadesEconomicas = ObtenerDatosFacturasOracle
					.cargaDatosActividadesEconomicasNexo();
			//
			int cantErrores = 0;
			//
			for (int lsa = 0; lsa < lotesSifenActivos.size(); lsa++) {
				// for (int lsa = 0; lsa < 1; lsa++) { //Enviar un solo lote por vez
				try {
					List<DocumentoElectronico> lote = new ArrayList<>();
					List<ParametrosFacturasNexoQuery> facturas = null;
					try {
						facturas = ObtenerDatosFacturasOracle
								.cargaDatosFacturacionElectronica(lotesSifenActivos.get(lsa).getLosiCodigo());
					} catch (SQLException ex) {
						try {
							Informe.ErrorLog("Error obtencion de datos de la DB, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
					} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
						try {
							Informe.ErrorLog("Error obtencion de datos de la DB, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
					} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError
							| StackOverflowError ex) {
						try {
							Informe.ErrorLog("Error obtencion de datos de la DB, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					// Loop del lote de facturas
					try {
						for (int i = 0; i < facturas.size(); i++) {
							if (facturas.get(i).getDetalleFacturaQuery().getDetalle().size() > 0) {
								facturas.get(i).setActividadesEconomicasNexoQuery(ActividadesEconomicas);
								lote.add(FacturasNexo2.facturaTest01(facturas.get(i)));
							} else {
								Informe.ErrorLog(
										"Factura electronica sin detalle: " + new Gson().toJson(facturas.get(i)));
							}
						}
					} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
						try {
							Informe.ErrorLog("Error Loop del lote de facturas, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
					} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError
							| StackOverflowError ex) {
						try {
							Informe.ErrorLog("Error Loop del lote de facturas, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
					} catch (Throwable ex) {
						// Capturando la excepción (superclase)
						try {
							Informe.ErrorLog("Error Loop del lote de facturas, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

					Informe.InfoLog("Datos del Lote en crudo: " + new Gson().toJson(facturas));
					Informe.InfoLog("Datos del Lote formateado: " + new Gson().toJson(lote));
					final RespuestaRecepcionLoteDE respuestaRecepcionLoteDE = Sifen.recepcionLoteDE(lote);
					SifenConfig sifenConfig = Sifen.getSifenConfig();
					for (int lot = 0; lot < lote.size(); lot++) {
						try {
							Informe.InfoLog("XML del CDC: " + lote.get(lot).obtenerCDC() + "\n"
									+ lote.get(lot).generarXml(GenerationCtx.getDefaultFromConfig(sifenConfig)) + "\n");
						} catch (NullPointerException err) {
							Informe.ErrorLog("Error al obtener el XMl del lote, motivo: " + err);
						}
						//
						try {
							writeXMLFile.saveXML(
									lote.get(lot).generarXml(GenerationCtx.getDefaultFromConfig(sifenConfig)),
									lote.get(lot).obtenerCDC(), "CDC");
						} catch (NullPointerException err) {
							Informe.ErrorLog("Error al obtener el XMl del lote, motivo: " + err);
						}
					}
					Informe.InfoLog("Respuesta: " + new Gson().toJson(respuestaRecepcionLoteDE));
					logger.info("Código de respuesta: " + respuestaRecepcionLoteDE.getdCodRes() + " - Fecha: "
							+ respuestaRecepcionLoteDE.getdFecProc());

					Informe.InfoLog("Codigo de Lote: " + respuestaRecepcionLoteDE.getdProtConsLote()
							+ " Codigo de Respuesta: *" + respuestaRecepcionLoteDE.getdCodRes()
							+ "* Mensaje de Respuesta: " + respuestaRecepcionLoteDE.getdMsgRes());
					//
					try {
						writeXMLFile.saveXML(
								respuestaRecepcionLoteDE.getRespuestaBruta(),
								respuestaRecepcionLoteDE.getdProtConsLote(), "LOTES_ENVIADOS");
					} catch (NullPointerException err) {
						Informe.ErrorLog("Error al obtener el XMl del lote, motivo: " + err);
					}

					Informe.InfoLog("RAW ANSWER " + respuestaRecepcionLoteDE.getRespuestaBruta());
					logger.info("RAW ANSWER " + respuestaRecepcionLoteDE.getRespuestaBruta());

					lotesSifenActivos.get(lsa).setdProtConsLote(respuestaRecepcionLoteDE.getdProtConsLote());
					lotesSifenActivos.get(lsa).setdCodRes(respuestaRecepcionLoteDE.getdCodRes());
					lotesSifenActivos.get(lsa).setdMsgRes(respuestaRecepcionLoteDE.getdMsgRes());

				} catch (SifenException e) {
					Informe.ErrorLog(
							"Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
					logger.error(
							"Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
				} catch (IOException | ArrayIndexOutOfBoundsException | NullPointerException ex) {
					try {
						Informe.ErrorLog("Error, motivo: " + ex);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
					try {
						Informe.ErrorLog("Error, motivo: " + ex);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				Informe.InfoLog("Datos para actualizar el lote: " + new Gson().toJson(lotesSifenActivos));

				if (lotesSifenActivos.get(lsa).getdCodRes() != null) {
					try {
						ObtenerDatosFacturasOracle.actualizaDatosLotesSifen(lotesSifenActivos.get(lsa).getLosiCodigo(),
								lotesSifenActivos.get(lsa).getdCodRes(), lotesSifenActivos.get(lsa).getdMsgRes(),
								lotesSifenActivos.get(lsa).getdProtConsLote());
					} catch (SQLException ex) {
						try {
							Informe.ErrorLog("Error en la actualizacion de los datos del lotes, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
					} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
						try {
							Informe.ErrorLog("Error, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
					} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError
							| StackOverflowError ex) {
						try {
							Informe.ErrorLog("Error, motivo: " + ex);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				} else {
					Informe.ErrorLog("********** POSIBLE ERROR EN EL SISTEMA DE LA SET **********");
					cantErrores += 1;
					if (cantErrores > 3) {
						cantErrores = 0;
						break; // Parar la ejecucion cuando es un error del sistema de SIFEN
					}
				}
			}
		} else {
			Informe.InfoLog("No existen lotes activos.");
		}

		Informe.InfoLog("==== ===== ======= ====");
		Informe.InfoLog("NEXO SIFEN ENVIO QUIT");
		Informe.InfoLog("==== ===== ======= ====");

	}

	public static void main(String[] args) throws IOException {

		try {
			SifenConfig sifenConfig = SifenConfig.cargarConfiguracion("conf/sifen-nexo.properties");
			envioSifen envioSifen = new envioSifen(sifenConfig);
			envioSifen.start();
		} catch (ClassNotFoundException | NoSuchMethodException | InterruptedException ex) {
			try {
				Informe.ErrorLog("Error, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (SifenException e) {
			// TODO Auto-generated catch block
			Informe.ErrorLog(
					"Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
			e.printStackTrace();
		}
	}
}
