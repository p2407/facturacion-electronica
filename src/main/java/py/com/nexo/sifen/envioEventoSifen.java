package py.com.nexo.sifen;

import com.google.gson.Gson;
import com.roshka.sifen.Sifen;
import com.roshka.sifen.core.SifenConfig;
import com.roshka.sifen.core.beans.EventosDE;
import com.roshka.sifen.core.beans.response.RespuestaRecepcionEvento;
import com.roshka.sifen.core.exceptions.SifenException;
import com.roshka.sifen.core.fields.request.event.TgGroupTiEvt;
import com.roshka.sifen.core.fields.request.event.TrGeVeCan;
import com.roshka.sifen.core.fields.request.event.TrGeVeInu;
import com.roshka.sifen.core.fields.request.event.TrGesEve;
import com.roshka.sifen.core.types.TTiDE;

import log.Informe;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class envioEventoSifen {
    private static Connection conexion;

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(envioEventoSifen.class);

    private envioEventoSifen(SifenConfig sifenConfig) throws IOException {
        try {
            Sifen.setSifenConfig(sifenConfig);
        } catch (SifenException e) {
            // TODO Auto-generated catch block
            Informe.ErrorLog(
                    "Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
        }
    }

    public static boolean crearConexionOracle() {
        boolean retorno = true;
        Properties propiedades = new Properties();
        try {
            propiedades.load(new FileReader("conf/datos_usuario_DB.properties"));
        } catch (IOException ex) {
            try {
                Informe.ErrorLog("Error en la conexión de la base de datos, motivo: " + ex.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        String baseConexion = propiedades.get("baseConexion").toString();
        String usuario = propiedades.get("user").toString();
        String contrasena = propiedades.get("password").toString();

        try {
            try {
                Informe.InfoLog("Creando conexion");
            } catch (IOException e) {
                e.printStackTrace();
            }
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conexion = DriverManager.getConnection(baseConexion, usuario, contrasena);
        } catch (SQLException | ClassNotFoundException ex) {
            try {
                Informe.ErrorLog(baseConexion + " Usuario: " + usuario + " Contrasena: " + contrasena);
                Informe.ErrorLog("Error en la conexión de la base de datos, motivo: " + ex.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        return retorno;
    }

    public static void cerrarConexion() {
        try {
            Informe.InfoLog("Cerrando conexion");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            conexion.close();
        } catch (SQLException ex) {
            try {
                Informe.ErrorLog("Error en la base de datos, motivo: " + ex.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static List<HashMap<String, String>> obtenerDatosDocumentosSinUsoAnulados() throws SQLException, IOException {
        boolean procesoExitoso = crearConexionOracle();
        if (!procesoExitoso) {
            return null;
        }
        Statement stmt = conexion.createStatement();
        List<HashMap<String, String>> elements = new ArrayList<HashMap<String, String>>();

        try {
            String query_select = "SELECT ROWNUM as ID_, (SELECT NVL(DATI_CODIGO, 0) FROM DATOS_TIMBRADO DATI WHERE ROWNUM <= 1) FAEL_NRO_TIMBRADO, '001' FAEL_ESTABLECIMIENTO, LPAD(SERI_CODIGO, 3, '0') FAEL_PUNTO_EXPEDICION, LPAD(SEDO_CODIGO, 7, '0') SEDO_CODIGO_INI, LPAD(SEDO_CODIGO, 7, '0') SEDO_CODIGO_FIN, CASE WHEN DOSIUS.TIDO_CODIGO = 22 THEN 1 WHEN DOSIUS.TIDO_CODIGO = 96 THEN 2 END TIPO_DOCUMENTO_ELECTRONICO, DOSIUS_OBSERVACION MOTIVO_EVENTO, SERI_CODIGO, TIDO_CODIGO, SEDO_CODIGO, PERS_CODIGO_EMISOR FROM PROD.DOCUMENTOS_SIN_USO_ANULADOS DOSIUS WHERE TRUNC(DOSIUS_FCH_INS) = TRUNC(SYSDATE) AND DOSIUS_ESTADO = 'AC' /*AND SEDO_CODIGO=296959*/";

            // Executing first SQL UPDATE query using executeUpdate() method of Statement
            // object.
            Informe.InfoLog("query_select =  " + query_select);
            // Executing SQL SELECT query using executeQuery() method of Statement object.
            ResultSet rs = stmt.executeQuery(query_select);

            // looping through the number of row/rows retrieved after executing SELECT
            // query3
            while (rs.next()) {
                HashMap<String, String> element = new HashMap<>();
                element.put("ID_", String.valueOf(rs.getInt("ID_")));
                element.put("FAEL_NRO_TIMBRADO", String.valueOf(rs.getInt("FAEL_NRO_TIMBRADO")));
                element.put("FAEL_ESTABLECIMIENTO", rs.getString("FAEL_ESTABLECIMIENTO"));
                element.put("FAEL_PUNTO_EXPEDICION", rs.getString("FAEL_PUNTO_EXPEDICION"));
                element.put("SEDO_CODIGO_INI", rs.getString("SEDO_CODIGO_INI"));
                element.put("SEDO_CODIGO_FIN", rs.getString("SEDO_CODIGO_FIN"));
                element.put("TIPO_DOCUMENTO_ELECTRONICO", String.valueOf(rs.getInt("TIPO_DOCUMENTO_ELECTRONICO")));
                element.put("MOTIVO_EVENTO", rs.getString("MOTIVO_EVENTO"));
                element.put("SERI_CODIGO", String.valueOf(rs.getInt("SERI_CODIGO")));
                element.put("TIDO_CODIGO", String.valueOf(rs.getInt("TIDO_CODIGO")));
                element.put("SEDO_CODIGO", String.valueOf(rs.getInt("SEDO_CODIGO")));
                element.put("PERS_CODIGO_EMISOR", String.valueOf(rs.getInt("PERS_CODIGO_EMISOR")));
                elements.add(element);
            }
            return elements;

        } catch (SQLException ex) {
            try {
                Informe.ErrorLog(
                        "Error en la base de datos al ejecutar el select, motivo: "
                                + ex.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        } finally {
            cerrarConexion();
        }

    }

    public static boolean actualizarDocumentosSinUsoAnulados(int seri_codigo, int tido_codigo, int sedo_codigo, int pers_codigo_emisor)
            throws SQLException, IOException {
        boolean procesoExitoso = crearConexionOracle();
        if (!procesoExitoso) {
            return false;
        }
        Statement stmt = conexion.createStatement();

        try {
            // First SQL UPDATE Query to update record.
            String query1 = "update prod.documentos_sin_uso_anulados dosius set dosius.dosius_estado = 'EN' where seri_codigo = "+seri_codigo+" and tido_codigo = "+tido_codigo+" and sedo_codigo = "+sedo_codigo+" and pers_codigo_emisor = "+pers_codigo_emisor+"";
            // Executing first SQL UPDATE query using executeUpdate() method of Statement
            // object.
            Informe.InfoLog("Query1 =  " + query1);
            int count = stmt.executeUpdate(query1);
            Informe.InfoLog("Numero de filas actualizadas ejecutando Query1 =  " + count);
            if (count > 0) {
                conexion.commit();
            } else {
                return false;
            }
        } catch (SQLException ex) {
            try {
                Informe.ErrorLog(
                        "Error en la base de datos al registrar los parametros o ejecutar el procedimiento, motivo: "
                                + ex.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        } finally {
            cerrarConexion();
        }

        return true;

    }

    public static boolean actualizarLote(int losiCodigo, String faelCdc, String nroTransaccion)
            throws SQLException, IOException {
        boolean procesoExitoso = crearConexionOracle();
        if (!procesoExitoso) {
            return false;
        }
        Statement stmt = conexion.createStatement();

        try {
            // First SQL UPDATE Query to update record.
            String query1 = "update lote_factura_sifen set lofasi_estado='Aprobado', numero_transaccion='"
                    + nroTransaccion + "' where losi_codigo = " + losiCodigo + " and fael_cdc = '" + faelCdc
                    + "' and lofasi_estado = 'Rechazado'";
            // Executing first SQL UPDATE query using executeUpdate() method of Statement
            // object.
            Informe.InfoLog("Query1 =  " + query1);
            int count = stmt.executeUpdate(query1);
            Informe.InfoLog("Numero de filas actualizadas ejecutando Query1 =  " + count);
            if (count > 0) {
                conexion.commit();
            } else {
                return false;
            }
        } catch (SQLException ex) {
            try {
                Informe.ErrorLog(
                        "Error en la base de datos al registrar los parametros o ejecutar el procedimiento, motivo: "
                                + ex.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        } finally {
            cerrarConexion();
        }

        return true;

    }

    private void doCheckSifenConfig() throws IOException {
        logger.info("Checking SIFEN CONFIG");
        final SifenConfig sifenConfig = Sifen.getSifenConfig();
        if (sifenConfig != null) {
            logger.info(sifenConfig.toString());
            try {
                Informe.InfoLog(sifenConfig.toString());
            } catch (IOException | ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
                try {
                    Informe.ErrorLog("Error, motivo: " + ex);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
                try {
                    Informe.ErrorLog("Error, motivo: " + ex);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            logger.error("envioSifen won't work. MUST SET SIFEN CONFIG FIRST");
            try {
                Informe.ErrorLog("envioSifen won't work. MUST SET SIFEN CONFIG FIRST");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void startEventoInutilizacion() throws IOException, SifenException, SQLException {

        Informe.InfoLog("======== ==== ===== ==========");
        Informe.InfoLog("Starting NEXO SIFEN ENVIO EVENTO INUTILIZACION...");
        Informe.InfoLog("======== ==== ===== ==========");

        doCheckSifenConfig();
        List<HashMap<String,String>> elements = obtenerDatosDocumentosSinUsoAnulados();
        for (int i = 0; i < elements.size(); i++) {
            HashMap<String, String> element = elements.get(i);
            
            String ID_ = element.get("ID_");
            int FAEL_NRO_TIMBRADO = Integer.parseInt(element.get("FAEL_NRO_TIMBRADO"));
            String FAEL_ESTABLECIMIENTO = element.get("FAEL_ESTABLECIMIENTO");
            String FAEL_PUNTO_EXPEDICION = element.get("FAEL_PUNTO_EXPEDICION");
            String SEDO_CODIGO_INI = element.get("SEDO_CODIGO_INI");
            String SEDO_CODIGO_FIN = element.get("SEDO_CODIGO_FIN");
            int TIPO_DOCUMENTO_ELECTRONICO = Integer.parseInt(element.get("TIPO_DOCUMENTO_ELECTRONICO"));
            String MOTIVO_EVENTO = element.get("MOTIVO_EVENTO");

            LocalDateTime currentDate = LocalDateTime.now();
            // Evento de Inutilizacion
            TrGeVeInu trGeVeInu = new TrGeVeInu();
            trGeVeInu.setdNumTim(FAEL_NRO_TIMBRADO);
            trGeVeInu.setdEst(FAEL_ESTABLECIMIENTO);
            trGeVeInu.setdPunExp(FAEL_PUNTO_EXPEDICION);
            trGeVeInu.setdNumIn(SEDO_CODIGO_INI);
            trGeVeInu.setdNumFin(SEDO_CODIGO_FIN);
            trGeVeInu.setiTiDE(TTiDE.getByVal((short) TIPO_DOCUMENTO_ELECTRONICO));
            trGeVeInu.setmOtEve(MOTIVO_EVENTO);
            //
            TgGroupTiEvt tgGroupTiEvt = new TgGroupTiEvt();
            tgGroupTiEvt.setrGeVeInu(trGeVeInu);
            //
            TrGesEve rGesEve1 = new TrGesEve();
            rGesEve1.setId(ID_);
            rGesEve1.setdFecFirma(currentDate);
            rGesEve1.setgGroupTiEvt(tgGroupTiEvt);
    
            EventosDE eventosDE = new EventosDE();
            eventosDE.setrGesEveList(Collections.singletonList(rGesEve1));
    
            RespuestaRecepcionEvento ret = Sifen.recepcionEvento(eventosDE);
            logger.info(ret.toString());
            Informe.InfoLog("Datos del Envio de Evento de Inutilizacion RespuestaBruta: " + ret.getRespuestaBruta());
            Informe.InfoLog("Datos del Envio de Evento de Inutilizacion: dProtAut: "
                    + ret.getgResProcEVe().get(0).getdProtAut() + " dEstRes: " + ret.getgResProcEVe().get(0).getdEstRes()
                    + " gResProc.dMsgRes: " + ret.getgResProcEVe().get(0).getgResProc().get(0).getdMsgRes());
            try {
                Informe.InfoLog("Respuesta: " + new Gson().toJson(ret));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (Integer.parseInt(ret.getgResProcEVe().get(0).getdProtAut()) > 0 && ret.getgResProcEVe().get(0).getdEstRes() == "Aprobado") {
                actualizarDocumentosSinUsoAnulados(Integer.parseInt(element.get("SERI_CODIGO")), Integer.parseInt(element.get("TIDO_CODIGO")), Integer.parseInt(element.get("SEDO_CODIGO")), Integer.parseInt(element.get("PERS_CODIGO_EMISOR")));
            }
        }

        Informe.InfoLog("==== ===== ======= ====");
        Informe.InfoLog("NEXO SIFEN ENVIO EVENTO INUTILIZACION QUIT");
        Informe.InfoLog("==== ===== ======= ====");

    }

    private void startEventoCancelacion(String cdc, String mOtEve) throws IOException, SifenException, SQLException {

        Informe.InfoLog("======== ==== ===== ==========");
        Informe.InfoLog("Starting NEXO SIFEN ENVIO EVENTO CANCELACION...");
        Informe.InfoLog("======== ==== ===== ==========");

        doCheckSifenConfig();
        //Motivo del evento
        //String mOtEve = "Nota de credito duplicada";
        //
        LocalDateTime currentDate = LocalDateTime.now();
        // Evento de Cancelación
        TrGeVeCan trGeVeCan = new TrGeVeCan();
        //trGeVeCan.setId("05800219600001001000119622022081818297609085");
        //trGeVeCan.setId("05800219600001001000162312023022819067444630");
        trGeVeCan.setId(cdc);
        trGeVeCan.setmOtEve(mOtEve);
        TgGroupTiEvt tgGroupTiEvt = new TgGroupTiEvt();
        tgGroupTiEvt.setrGeVeCan(trGeVeCan);
        
        TrGesEve rGesEve1 = new TrGesEve();
        rGesEve1.setId("1");
        rGesEve1.setdFecFirma(currentDate);
        rGesEve1.setgGroupTiEvt(tgGroupTiEvt);

        EventosDE eventosDE = new EventosDE();
        eventosDE.setrGesEveList(Collections.singletonList(rGesEve1));

        RespuestaRecepcionEvento ret = Sifen.recepcionEvento(eventosDE);
        Informe.InfoLog(ret.toString());
        Informe.InfoLog("Datos del Envio de Evento de Cancelacion RespuestaBruta: " + ret.getRespuestaBruta());
        Informe.InfoLog("Datos del Envio de Evento de Cancelacion: dProtAut: "
                    + ret.getgResProcEVe().get(0).getdProtAut() + " dEstRes: " + ret.getgResProcEVe().get(0).getdEstRes()
                    + " gResProc.dMsgRes: " + ret.getgResProcEVe().get(0).getgResProc().get(0).getdMsgRes());
        try {
            Informe.InfoLog("Respuesta: " + new Gson().toJson(ret));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Informe.InfoLog("==== ===== ======= ====");
        Informe.InfoLog("NEXO SIFEN ENVIO EVENTO CANCELACION QUIT");
        Informe.InfoLog("==== ===== ======= ====");

    }

    public static void main(String[] args) throws IOException, SifenException {

        SifenConfig sifenConfig = SifenConfig.cargarConfiguracion("conf/sifen-nexo.properties");
        envioEventoSifen envioSifen = new envioEventoSifen(sifenConfig);
        try {
            envioSifen.startEventoInutilizacion();
            // envioSifen.startEventoCancelacion("05800219600001003000052922025010218237304109", "MALA IMPUTACION DE NOTA DE CREDITO");
        } catch (SQLException ex) {
            try {
                Informe.ErrorLog("Error, motivo: " + ex);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
