package py.com.nexo.sifen;

import com.roshka.sifen.Sifen;
import com.roshka.sifen.core.SifenConfig;
import com.roshka.sifen.core.beans.DocumentoElectronico;
import com.roshka.sifen.core.beans.response.RespuestaConsultaLoteDE;
import com.roshka.sifen.core.beans.response.RespuestaConsultaRUC;
import com.roshka.sifen.core.beans.response.RespuestaRecepcionLoteDE;
import com.roshka.sifen.core.exceptions.SifenException;
import com.roshka.sifen.internal.util.SifenUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SifenManager {
    
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SifenManager.class);

    private boolean keepRunning;

    private SifenManager(SifenConfig sifenConfig) {
        keepRunning = true;
        try {
            Sifen.setSifenConfig(sifenConfig);
        } catch (SifenException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void stop() {
        keepRunning = false;
    }

    private void doCheckSifenConfig() {
        logger.info("Checking SIFEN CONFIG");
        final SifenConfig sifenConfig = Sifen.getSifenConfig();
        if (sifenConfig != null) {
            logger.info(sifenConfig.toString());
        } else {
            logger.error("SifenManager won't work. MUST SET SIFEN CONFIG FIRST");
        }

    }

    private void start() {

        logger.info("======== ==== ===== ==========");
        logger.info("Starting NEXO SIFEN MANAGER...");
        logger.info("======== ==== ===== ==========");

        doCheckSifenConfig();

        while (keepRunning) {

            // 1. check if there are pending INVOICES to send @ Oracle Database

            // 2. Send it to SIFEN



            // PRUEBA CONSULTA RUC
            try {

                final RespuestaConsultaRUC respuestaConsultaRUC = Sifen.consultaRUC("980527");
                logger.info("Consulta: " + respuestaConsultaRUC.getdCodRes());
            } catch (SifenException e) {
                logger.error("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
            }

            String dProtConsLote = null;

            // PRUEBA ENVÍO LOTE
            try {
                List<DocumentoElectronico> lote = new ArrayList<>();
                lote.add(FacturasNexo.facturaTest01());
                final RespuestaRecepcionLoteDE respuestaRecepcionLoteDE = Sifen.recepcionLoteDE(lote);
                logger.info("Código de respuesta: " + respuestaRecepcionLoteDE.getdCodRes() + " - Fecha: " + respuestaRecepcionLoteDE.getdFecProc());

                logger.info("RAW ANSWER" + respuestaRecepcionLoteDE.getRespuestaBruta());

                dProtConsLote = respuestaRecepcionLoteDE.getdProtConsLote();

            } catch (SifenException e) {
                logger.error("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
            }


            if (dProtConsLote != null) {
                logger.info("Consultando LOTE");

                try {
                    final RespuestaConsultaLoteDE respuestaConsultaLoteDE = Sifen.consultaLoteDE(dProtConsLote);
                    logger.info("Respuesta: " + respuestaConsultaLoteDE.getdCodRes());
                } catch (SifenException e) {
                    logger.error("Hubo un error en la librería SIFEN. Código: " + e.getCode() + " (" + e.getMessage() + ")");
                }


            }

            keepRunning = false;


        }

        logger.info("==== ===== ======= ====");
        logger.info("NEXO SIFEN MANAGER QUIT");
        logger.info("==== ===== ======= ====");

    }

    public static void main(String[] args) throws IOException {

        SifenConfig sifenConfig;
        try {
            sifenConfig = SifenConfig.cargarConfiguracion("conf/sifen-nexo.properties");
            SifenManager sifenManager = new SifenManager(sifenConfig);
            sifenManager.start();
        } catch (SifenException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
