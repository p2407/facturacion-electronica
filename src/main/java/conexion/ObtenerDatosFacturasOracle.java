/**
 * 
 */
package conexion;

import java.io.*;
import java.sql.*;
import java.util.List;
import java.util.Properties;

import com.google.gson.Gson;
import com.roshka.sifen.core.fields.response.batch.TgResProcLote;

import converter.ObjectToActividadesEconomicasNexoConverter;
import converter.ObjectToConsultaLotesSifenEntregadosQueryConverter;
import converter.ObjectToDetalleFacturaQueryConverter;
import converter.ObjectToFormaPagoConverter;
import converter.ObjectToLotesSifenQueryConverter;
import converter.ObjectToParametrosFacturaNexoConverter;
import log.Informe;
import oracle.jdbc.*;
import querys.ActividadesEconomicasNexoQuery;
import querys.ConsultaLotesSifenEntregadosQuery;
import querys.DetalleDetalleFacturaQuery;
import querys.DetalleFacturaQuery;
import querys.FormaPago;
import querys.LotesSifenQuery;
import querys.ParametrosFacturasNexoQuery;

/**
 * @author Luis Cabral
 *
 */
public class ObtenerDatosFacturasOracle {
	private static Connection conexion;

	public static void main(String args[]) throws SQLException {
		System.out.println(cargaDatosFacturacionElectronica((long) 1));
	}

	public static boolean crearConexionOracle() {
		boolean retorno = true;
		Properties propiedades = new Properties();
		try {
			propiedades.load(new FileReader("conf/datos_usuario_DB.properties"));
		} catch (IOException ex) {
			try {
				Informe.ErrorLog("Error en la conexión de la base de datos, motivo: " + ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}

		String baseConexion = propiedades.get("baseConexion").toString();
		String usuario = propiedades.get("user").toString();
		String contrasena = propiedades.get("password").toString();

		try {
			try {
				Informe.InfoLog("Creando conexion");
			} catch (IOException e) {
				e.printStackTrace();
			}
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conexion = DriverManager.getConnection(baseConexion, usuario, contrasena);
		} catch (SQLException | ClassNotFoundException ex) {
			try {
				Informe.ErrorLog(baseConexion + " Usuario: " + usuario + " Contrasena: " + contrasena);
				Informe.ErrorLog("Error en la conexión de la base de datos, motivo: " + ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}

		return retorno;
	}

	public static void cerrarConexion() {
		try {
			Informe.InfoLog("Cerrando conexion");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			conexion.close();
		} catch (SQLException ex) {
			try {
				Informe.ErrorLog("Error en la base de datos, motivo: " + ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static List<ParametrosFacturasNexoQuery> cargaDatosFacturacionElectronica(Long losiCodigo)
			throws SQLException {
		boolean procesoExitoso = crearConexionOracle();
		if (!procesoExitoso) {
			return null;
		}
		CallableStatement cst = null;
		List<ParametrosFacturasNexoQuery> entity = null;
		// Llamada al procedimiento almacenado
		try {
			try {
				Informe.InfoLog("************* Iniciando proc_retorna_datos_factura *************");
			} catch (IOException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex.getMessage());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// Registra al procedimiento almacenado
			cst = conexion.prepareCall("{call proc_retorna_datos_factura (?, ?)}");

			// Registra el o los parametros de entrada
			cst.setLong(1, losiCodigo); // losi_codigo

			// Registra el o los parametros de salida
			cst.registerOutParameter(2, OracleTypes.CURSOR);

			// Ejecuta el procedimiento almacenado
			cst.execute();

			ResultSet rst = ((OracleCallableStatement) cst).getCursor(2);

			entity = ObjectToParametrosFacturaNexoConverter.convertToParametrosFacturaNexoQuery(rst, losiCodigo);

			// Loop del detalle
			for (int i = 0; i < entity.size(); i++) {
				entity.get(i).setDetalleFacturaQuery(
						cargaDatosFacturacionElectronicaDetalle(conexion, entity.get(i).getFaelCDC()));
			}

		} catch (SQLException ex) {
			try {
				Informe.ErrorLog(
						"Error en la base de datos al registrar los parametros o ejecutar el procedimiento, motivo: "
								+ ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		} finally {
			cerrarConexion();
		}

		try {
			Informe.InfoLog(new Gson().toJson(entity));
		} catch (IOException ex) {
			try {
				Informe.ErrorLog("Error en la consulta de los lotes activos, motivo: " + ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return entity;
	}

	static DetalleFacturaQuery cargaDatosFacturacionElectronicaDetalle(Connection conexion, String pFaelCDC)
			throws SQLException {
		CallableStatement cst = null;
		DetalleFacturaQuery entity = new DetalleFacturaQuery();
		List<DetalleDetalleFacturaQuery> detalle = null;
		List<FormaPago> FormaPago = null;
		// Llamada al procedimiento almacenado
		try {
			cst = conexion.prepareCall("{call proc_retorna_datos_factura_det (?, ? ,?)}");
		} catch (SQLException ex) {
			try {
				Informe.ErrorLog(
						"Error en la base de datos proc_retorna_datos_factura_det, motivo: " + ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		// Ejecuta el procedimiento almacenado

		try {
			cst.setString(1, pFaelCDC); // FAEL_CDC
			cst.registerOutParameter(2, OracleTypes.CURSOR);
			cst.registerOutParameter(3, OracleTypes.CURSOR);
			cst.execute();
		} catch (SQLException ex) {
			try {
				Informe.ErrorLog(
						"Error en la base de datos al registrar los parametros o ejecutar el procedimiento proc_retorna_datos_factura_det, motivo: "
								+ ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		try {

			ResultSet rst = ((OracleCallableStatement) cst).getCursor(2);
			ResultSet rst2 = ((OracleCallableStatement) cst).getCursor(3);

			detalle = ObjectToDetalleFacturaQueryConverter.convertToDetalleFacturaQuery(rst);
			FormaPago = ObjectToFormaPagoConverter.convertToFormaPago(rst2);

			entity.setDetalle(detalle);
			entity.setFormaPago(FormaPago);

		} catch (SQLException ex) {
			try {
				Informe.ErrorLog("Error al traer los datos, motivo: " + ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		return entity;
	}

	public static List<ActividadesEconomicasNexoQuery> cargaDatosActividadesEconomicasNexo() {
		boolean procesoExitoso = crearConexionOracle();
		if (!procesoExitoso) {
			return null;
		}
		CallableStatement cst = null;
		List<ActividadesEconomicasNexoQuery> entity = null;

		try {
			try {
				Informe.InfoLog("************* Iniciando proc_retorna_actividades_econ_nexo *************");
			} catch (IOException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex.getMessage());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// Registra al procedimiento almacenado
			cst = conexion.prepareCall("{call proc_retorna_actividades_econ_nexo (?)}");

			// Registra el o los parametros de salida
			cst.registerOutParameter(1, OracleTypes.CURSOR);

			// Ejecuta el procedimiento almacenado
			cst.execute();

			ResultSet rst = ((OracleCallableStatement) cst).getCursor(1);

			entity = ObjectToActividadesEconomicasNexoConverter.convertToActividadesEconomicasNexoQuery(rst);
		} catch (SQLException ex) {
			try {
				Informe.ErrorLog(
						"Error en la base de datos al registrar los parametros o ejecutar el procedimiento, motivo: "
								+ ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		} finally {
			cerrarConexion();
		}
		try {
			Informe.InfoLog(new Gson().toJson(entity));
		} catch (IOException ex) {
			try {
				Informe.ErrorLog("Error en la consulta de los lotes activos, motivo: " + ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return entity;
	}

	public static List<LotesSifenQuery> cargaDatosLotesSifen() throws SQLException {
		List<LotesSifenQuery> entity = null;
		CallableStatement cst = null;

		boolean procesoExitoso = crearConexionOracle();
		if (!procesoExitoso) {
			return null;
		}

		try {
			try {
				Informe.InfoLog("************* Iniciando proc_retorna_lotes_sifen_activos *************");
			} catch (IOException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex.getMessage());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// Registra al procedimiento almacenado
			cst = conexion.prepareCall("{call proc_retorna_lotes_sifen_activos (?)}");

			// Registra el o los parametros de salida
			cst.registerOutParameter(1, OracleTypes.CURSOR);

			// Ejecuta el procedimiento almacenado
			cst.execute();

			ResultSet rst = ((OracleCallableStatement) cst).getCursor(1);

			entity = ObjectToLotesSifenQueryConverter.convertToLotesSifenQuery(rst);

		} catch (SQLException ex) {
			try {
				Informe.ErrorLog(
						"Error en la base de datos al registrar los parametros o ejecutar el procedimiento, motivo: "
								+ ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		} finally {
			cerrarConexion();
		}

		try {
			Informe.InfoLog(new Gson().toJson(entity));
		} catch (IOException ex) {
			try {
				Informe.ErrorLog("Error en la consulta de los lotes activos, motivo: " + ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return entity;
	}

	public static void actualizaDatosLotesSifen(Long losiCodigo, String codigoRespuesta, String mensajeRespuesta,
			String codigoLoteRespuesta) throws SQLException {
		CallableStatement cst = null;

		boolean procesoExitoso = crearConexionOracle();
		if (!procesoExitoso) {
			return;
		}

		try {
			try {
				Informe.InfoLog("************* Iniciando proc_actualiza_lotes_sifen *************");
				Informe.InfoLog(" losiCodigo: " + losiCodigo + " codigoRespuesta: " + codigoRespuesta
						+ " mensajeRespuesta: " + mensajeRespuesta + " codigoLoteRespuesta: " + codigoLoteRespuesta);
			} catch (IOException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex.getMessage());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// Registra al procedimiento almacenado
			cst = conexion.prepareCall("{call proc_actualiza_lotes_sifen (?, ?, ?, ?)}");

			// Registra el o los parametros de entrada
			cst.setLong(1, losiCodigo); // LOSI_CODIGO
			cst.setString(2, codigoRespuesta); // CODIGO_RESPUESTA
			cst.setString(3, mensajeRespuesta); // MENSAJE_RESPUESTA
			cst.setString(4, codigoLoteRespuesta); // CODIGO_LOTE_RESPUESTA

			// Registra el o los parametros de salida
			// cst.registerOutParameter(1, OracleTypes.CURSOR);

			// Ejecuta el procedimiento almacenado
			cst.execute();

		} catch (SQLException ex) {
			try {
				Informe.ErrorLog(
						"Error en la base de datos al registrar los parametros o ejecutar el procedimiento, motivo: "
								+ ex.getMessage());
				Informe.ErrorLog("codigoLoteRespuesta: " + losiCodigo);
				Informe.ErrorLog("codigoRespuesta: " + codigoRespuesta);
				Informe.ErrorLog("mensajeRespuesta: " + mensajeRespuesta);
				Informe.ErrorLog("codigoLoteRespuesta: " + codigoLoteRespuesta);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		} finally {
			cerrarConexion();
		}

	}

	public static List<ConsultaLotesSifenEntregadosQuery> consultaLotesSifenEntregados() throws SQLException {
		List<ConsultaLotesSifenEntregadosQuery> entity = null;
		CallableStatement cst = null;

		boolean procesoExitoso = crearConexionOracle();
		if (!procesoExitoso) {
			return null;
		}

		try {
			try {
				Informe.InfoLog("************* Iniciando proc_consulta_lotes_entregados *************");
			} catch (IOException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex.getMessage());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// Registra al procedimiento almacenado
			cst = conexion.prepareCall("{call proc_consulta_lotes_entregados (?)}");

			// Registra el o los parametros de salida
			cst.registerOutParameter(1, OracleTypes.CURSOR);

			// Ejecuta el procedimiento almacenado
			cst.execute();

			ResultSet rst = ((OracleCallableStatement) cst).getCursor(1);

			entity = ObjectToConsultaLotesSifenEntregadosQueryConverter
					.converterToConsultaLotesSifenEntregadosQuery(rst);

		} catch (SQLException ex) {
			try {
				Informe.ErrorLog(
						"Error en la base de datos al registrar los parametros o ejecutar el procedimiento, motivo: "
								+ ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		} finally {
			cerrarConexion();
		}

		try {
			Informe.InfoLog(new Gson().toJson(entity));
		} catch (IOException ex) {
			try {
				Informe.ErrorLog("Error en la consulta de los lotes activos, motivo: " + ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return entity;
	}

	public static void actualizaLotesEntregadosSifen(String dProtConsLote, List<TgResProcLote> gResProcLoteList)
			throws SQLException {
		CallableStatement cst = null;

		boolean procesoExitoso = crearConexionOracle();
		if (!procesoExitoso) {
			return;
		}

		try {
			try {
				Informe.InfoLog("************* Iniciando proc_actualiza_lotes_entregados *************");
			} catch (IOException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex.getMessage());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// Registra al procedimiento almacenado
			cst = conexion.prepareCall("{call proc_actualiza_lotes_entregados (?, ?, ?, ?)}");

			for (int i = 0; i < gResProcLoteList.size(); i++) {
				// Registra el o los parametros de entrada
				cst.setString(1, dProtConsLote); // p_codigo_lote_sifen
				cst.setString(2, gResProcLoteList.get(i).getId()); // p_fael_cdc
				cst.setString(3, gResProcLoteList.get(i).getdEstRes()); // p_estado_respuesta
				cst.setString(4, gResProcLoteList.get(i).getdProtAut()); // p_d_Prot_Aut

				// Registra el o los parametros de salida
				// cst.registerOutParameter(1, OracleTypes.CURSOR);

				// Ejecuta el procedimiento almacenado
				cst.execute();

				for (int d = 0; d < gResProcLoteList.get(i).getgResProc().size(); d++) {
					insertarGrupoMsgRespueta(conexion, dProtConsLote, gResProcLoteList.get(i).getId(),
							gResProcLoteList.get(i).getgResProc().get(d).getdCodRes(),
							gResProcLoteList.get(i).getgResProc().get(d).getdMsgRes());
				}
			}

		} catch (SQLException ex) {
			try {
				Informe.ErrorLog(
						"Error en la base de datos al registrar los parametros o ejecutar el procedimiento, motivo: "
								+ ex.getMessage());
				Informe.ErrorLog("gResProcLoteList: " + new Gson().toJson(gResProcLoteList));
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		} finally {
			cerrarConexion();
		}

	}

	static void insertarGrupoMsgRespueta(Connection conexion, String dProtConsLote, String faelCDC, String dCodRes,
			String dMsgRes) throws SQLException {
		CallableStatement cst = null;
		try {
			// Registra al procedimiento almacenado
			cst = conexion.prepareCall("{call proc_inserta_grupo_msg_resultado (?, ?, ?, ?)}");

			// Registra el o los parametros de entrada
			cst.setString(1, dProtConsLote); // p_codigo_lote_sifen
			cst.setString(2, faelCDC); // p_fael_cdc
			cst.setString(3, dCodRes); // p_d_Cod_Res
			cst.setString(4, dMsgRes); // p_msj_respuesta

			// Registra el o los parametros de salida
			// cst.registerOutParameter(1, OracleTypes.CURSOR);

			// Ejecuta el procedimiento almacenado
			cst.execute();

		} catch (SQLException ex) {
			try {
				Informe.ErrorLog(
						"Error en la base de datos al registrar los parametros o ejecutar el procedimiento, motivo: "
								+ ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}

	}

	public static void actualizaLotesRechazadosSifen(Long losiCodigo, String faelCDC, String dEstRes, String dCodRes, String dMsgRes) throws SQLException {
		CallableStatement cst = null;

		boolean procesoExitoso = crearConexionOracle();
		if (!procesoExitoso) {
			return;
		}
		try {
			try {
				Informe.InfoLog("************* Iniciando proc_actualiza_lotes_rechazado *************");
			} catch (IOException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex.getMessage());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// Registra al procedimiento almacenado
			cst = conexion.prepareCall("{call proc_actualiza_lotes_rechazado (?, ?, ?, ?, ?)}");

			// Registra el o los parametros de entrada
			cst.setLong(1, losiCodigo); // p_losi_codigo
			cst.setString(2, faelCDC); // p_fael_cdc
			cst.setString(3, dEstRes); // p_estado_respuesta
			cst.setString(4, dCodRes); // p_d_Cod_Res
			cst.setString(5, dMsgRes); // p_msj_respuesta

			// Registra el o los parametros de salida
			// cst.registerOutParameter(1, OracleTypes.CURSOR);

			// Ejecuta el procedimiento almacenado
			cst.execute();

		} catch (SQLException ex) {
			try {
				Informe.ErrorLog(
						"Error en la base de datos al registrar los parametros o ejecutar el procedimiento, motivo: "
								+ ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		} finally {
			cerrarConexion();
		}

	}

	public static Integer retorna_cantidad_faltantes_de_consulta() {
		boolean procesoExitoso = crearConexionOracle();
		if (!procesoExitoso) {
			return null;
		}
		CallableStatement cst = null;
		Integer resultado = null;

		try {
			try {
				Informe.InfoLog("************* Iniciando func_fael_faltantes_de_consulta *************");
			} catch (IOException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex.getMessage());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// Registra al procedimiento almacenado
			cst = conexion.prepareCall("{ ? = call func_fael_faltantes_de_consulta() }");

			// Registra el o los parametros de salida
			cst.registerOutParameter(1, Types.INTEGER);

			// Ejecuta el procedimiento almacenado
			cst.execute();

			resultado = cst.getInt(1);

		} catch (SQLException ex) {
			try {
				Informe.ErrorLog(
						"Error en la base de datos al registrar los parametros o ejecutar el procedimiento, motivo: "
								+ ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		} finally {
			cerrarConexion();
		}

		return resultado;
	}

	static void procCargarLotesSifen() throws SQLException {
		boolean procesoExitoso = crearConexionOracle();
		if (!procesoExitoso) {
			return;
		}
		CallableStatement cst = null;

		try {
			try {
				Informe.InfoLog("************* Iniciando proc_cargar_lotes_sifen *************");
			} catch (IOException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex.getMessage());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// Registra al procedimiento almacenado
			cst = conexion.prepareCall("{call proc_cargar_lotes_sifen ()}");

			// Ejecuta el procedimiento almacenado
			cst.execute();

		} catch (SQLException ex) {
			try {
				Informe.ErrorLog(
						"Error en la base de datos al registrar los parametros o ejecutar el procedimiento, motivo: "
								+ ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		} finally {
			cerrarConexion();
		}

	}

	public static List<ParametrosFacturasNexoQuery> obtenerFacturasACancelar() throws SQLException {
		boolean procesoExitoso = crearConexionOracle();
		if (!procesoExitoso) {
			return null;
		}
		List<ParametrosFacturasNexoQuery> entity = null;
		CallableStatement cst = null;

		try {
			try {
				Informe.InfoLog("************* Iniciando proc_obtener_fael_a_cancelar *************");
			} catch (IOException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex.getMessage());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			// Registra al procedimiento almacenado
			cst = conexion.prepareCall("{call proc_obtener_fael_a_cancelar (?)}");

			// Registra el o los parametros de salida
			cst.registerOutParameter(1, OracleTypes.CURSOR);

			// Ejecuta el procedimiento almacenado
			cst.execute();

			ResultSet rst = ((OracleCallableStatement) cst).getCursor(1);

			entity = ObjectToParametrosFacturaNexoConverter.convertToFacturasInvalidasQuery(rst);

		} catch (SQLException ex) {
			try {
				Informe.ErrorLog(
						"Error en la base de datos al registrar los parametros o ejecutar el procedimiento, motivo: "
								+ ex.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		} finally {
			cerrarConexion();
		}

		return entity;

	}
}
