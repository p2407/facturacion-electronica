package conexion;

import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.time.*;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.*;

import com.roshka.sifen.core.exceptions.SifenException;

import log.Informe;
import py.com.nexo.sifen.*;

public class EjecucionRepetitiva {
	static ScheduledFuture<?> scheduledFuture = null;

	static Integer horaCorrida = 23;
	static Integer minutoCorrida = 0;
	static Integer periodicDelay = 0;
	static String unidadDeTiempo = "MINUTES";
	static Integer cantidadFaltanteDeConsulta = 1;

	private static Scanner lectura;

	public static class RunnableTask implements Runnable {
		@Override
		public void run() {
			try {
				try {
					Informe.InfoLog("************* Inicia la generacion de los lotes *************");
				} catch (IOException ex) {
					try {
						Informe.ErrorLog("Error, motivo: " + ex.getMessage());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				ObtenerDatosFacturasOracle.procCargarLotesSifen();
			} catch (SQLException | ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			try {
				envioSifen.main(null);
			} catch (IOException ex) {
				try {
					Informe.ErrorLog("Error al ejecutar envioSifen, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			cantidadFaltanteDeConsulta = ObtenerDatosFacturasOracle.retorna_cantidad_faltantes_de_consulta();
			if (cantidadFaltanteDeConsulta > 0) {
				try {
					Informe.InfoLog("************* Tiempo de procesamiento de la totalidad del lote: "
							+ cantidadFaltanteDeConsulta + " minutos *************");
				} catch (IOException ex) {
					try {
						Informe.ErrorLog("Error, motivo: " + ex);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				try {
					TimeUnit.MINUTES.sleep(cantidadFaltanteDeConsulta);
				} catch (InterruptedException ex) {
					try {
						Informe.ErrorLog("Error, motivo: " + ex);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
					try {
						Informe.ErrorLog("Error, motivo: " + ex);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
					try {
						Informe.ErrorLog("Error, motivo: " + ex);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				try {
					consultaSifen.main(null);
				} catch (IOException | NoSuchMethodException | ClassNotFoundException | InterruptedException ex) {
					try {
						Informe.ErrorLog("Error al ejecutar la consultaSifen, motivo: " + ex);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
					try {
						Informe.ErrorLog("Error, motivo: " + ex);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
					try {
						Informe.ErrorLog("Error, motivo: " + ex);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else {
				try {
					Informe.InfoLog("************* No exiten lotes faltantes de consulta *************");
				} catch (IOException ex) {
					try {
						Informe.ErrorLog("Error, motivo: " + ex);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			try {
				consultaSifenPorCdc.main(null);
			} catch (IOException | NoSuchMethodException | ClassNotFoundException | InterruptedException | SifenException ex) {
				try {
					Informe.ErrorLog("Error al ejecutar la consultaSifen, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError | SQLException ex) {
				try {
					Informe.ErrorLog("Error, motivo: " + ex);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	};

	private static void startSimple() {

		RunnableTask task2 = new RunnableTask();
		task2.run();

		/*
		 * ScheduledExecutorService scheduler =
		 * Executors.newSingleThreadScheduledExecutor();
		 * 
		 * Runnable task = new RunnableTask(); int initialDelay = 0; int periodicDelay =
		 * 3; scheduler.scheduleAtFixedRate(task, initialDelay, periodicDelay,
		 * TimeUnit.MINUTES );
		 */
	}

	private static void iniciarConsultaCdcDuplicados() {

		try {
			consultaSifenPorCdc.IniciarRemoto();
		} catch (IOException | SifenException | SQLException ex) {
			try {
				Informe.ErrorLog("Error al obtener las propiedades datos_usuario_DB, motivo: " + ex);
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void startSimpleDelay() {
		ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

		Runnable task = new RunnableTask();
		int initialDelay = 0;
		scheduler.scheduleAtFixedRate(task, initialDelay, periodicDelay, TimeUnit.valueOf(unidadDeTiempo));
	}

	private static void start() {
		ZonedDateTime now = ZonedDateTime.now(ZoneId.of("America/Asuncion"));
		ZonedDateTime nextRun = now.withHour(horaCorrida).withMinute(minutoCorrida).withSecond(0); // Todos los dias a
																									// las 11 de la
																									// noche
		if (now.compareTo(nextRun) > 0)
			nextRun = nextRun.plusDays(1);

		Duration duration = Duration.between(now, nextRun);
		long initalDelay = duration.getSeconds();

		ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
		scheduledFuture = scheduler.scheduleAtFixedRate(new RunnableTask(), initalDelay, TimeUnit.DAYS.toSeconds(1),
				TimeUnit.SECONDS);
	}

	public static void main(String[] args) {
		Properties propiedades = new Properties();
		try {
			propiedades.load(new FileReader("conf/datos_usuario_DB.properties"));
		} catch (IOException ex) {
			try {
				Informe.ErrorLog("Error al obtener las propiedades datos_usuario_DB, motivo: " + ex);
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
			try {
				Informe.ErrorLog("Error, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
			try {
				Informe.ErrorLog("Error, motivo: " + ex);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//
		String modo = "";
		//
		horaCorrida = Integer.parseInt(propiedades.get("horaCorrida").toString());
		minutoCorrida = Integer.parseInt(propiedades.get("minutoCorrida").toString());
		periodicDelay = Integer.parseInt(propiedades.get("periodicDelay").toString());
		unidadDeTiempo = propiedades.get("unidadDeTiempo").toString();
		//
		if (args.length > 0) {
			System.out.println("modo de uso: " + args[0]);
			modo = args[0];
		} else {
			//
			lectura = new Scanner(System.in);
			System.out
					.println(
							"Ingrese modo de uso: -> 1: Ejecucion de unica vez, 2: Ejecucion a las 23:00 todos los dias ");
			System.out.println("1: Ejecucion de unica vez");
			System.out.println("2: Ejecucion a las " + horaCorrida + ":" + minutoCorrida + " todos los dias");
			System.out.println("3: Ejecutar cada " + periodicDelay + " " + unidadDeTiempo);
			System.out.println("4: Iniciar Consulta Rechazados por CDC Duplicados");
			modo = lectura.next();
		}
		if (modo.equals("2")) {
			System.out.println(
					"2: Ejecucion a las " + horaCorrida + ":" + minutoCorrida + " todos los dias [Seleccionada]");
			start();
		} else if (modo.equals("3")) {
			System.out.println("3: Ejecutar cada " + periodicDelay + " " + unidadDeTiempo + " [Seleccionada]");
			startSimpleDelay();
		} else if (modo.equals("4")) {
			System.out.println("4: Iniciar Consulta Rechazados por CDC Duplicados [Seleccionada]");
			iniciarConsultaCdcDuplicados();
		} else {
			System.out.println("1: Ejecucion de unica vez [Seleccionada]");
			startSimple();
		}
	}
}
