package converter;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import log.Informe;
import querys.DetalleDetalleFacturaQuery;

public class ObjectToDetalleFacturaQueryConverter {
	
	public static List<DetalleDetalleFacturaQuery> convertToDetalleFacturaQuery (ResultSet resultado) {
		List<DetalleDetalleFacturaQuery> facturasDetalle = new ArrayList<DetalleDetalleFacturaQuery>();
		DetalleDetalleFacturaQuery facturaDetalle = null;
		
		try {
			while (resultado.next()) {
				facturaDetalle = new DetalleDetalleFacturaQuery();
				facturaDetalle.setdCodInt(resultado.getString("COD_INT"));
				facturaDetalle.setdDesProSer(resultado.getString("DESC_PRO_SER"));
				facturaDetalle.setdPUniProSer(resultado.getBigDecimal("FAELDE_MONTO"));
				facturaDetalle.setiAfecIVA(resultado.getInt("AFEC_IVA"));
				facturaDetalle.setdPropIVA(resultado.getBigDecimal("PROP_IVA"));
				facturaDetalle.setdTasaIVA(resultado.getBigDecimal("TASA_IVA"));
				
				facturasDetalle.add(facturaDetalle);
				
			}
		} catch (SQLException e) {
			try {
				Informe.ErrorLog("Error al convertir los datos, motivo: " + e.getMessage());
			} catch (IOException er) {
				er.printStackTrace();
			}
		}
		
		return facturasDetalle;
	}

}
