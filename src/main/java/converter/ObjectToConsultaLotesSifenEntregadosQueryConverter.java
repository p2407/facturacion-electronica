package converter;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import log.Informe;
import querys.ConsultaLotesSifenEntregadosQuery;

public class ObjectToConsultaLotesSifenEntregadosQueryConverter {
	
	public static List<ConsultaLotesSifenEntregadosQuery> converterToConsultaLotesSifenEntregadosQuery(ResultSet resultado){
		List<ConsultaLotesSifenEntregadosQuery> lotesEntregados = new ArrayList<ConsultaLotesSifenEntregadosQuery>();
		ConsultaLotesSifenEntregadosQuery codigoLote = null;
		
		try {
			while (resultado.next()) {
				codigoLote = new ConsultaLotesSifenEntregadosQuery();
				
				codigoLote.setdProtConsLote(resultado.getString("CODIGO_LOTE_SIFEN"));
				
				lotesEntregados.add(codigoLote);
			}
		} catch (SQLException error) {
			try {
				Informe.ErrorLog("Error al convertir los datos, motivo: " + error.getMessage());
			} catch (IOException er) {
				er.printStackTrace();
			}
		}
		
		return lotesEntregados;
	}

}
