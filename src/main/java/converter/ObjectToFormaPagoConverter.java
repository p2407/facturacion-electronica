package converter;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import log.Informe;
import querys.FormaPago;

public class ObjectToFormaPagoConverter {
	
	public static List<FormaPago> convertToFormaPago(ResultSet resultado) {
		List<FormaPago> formasDePago = new ArrayList<FormaPago>();
		FormaPago formaPago = null;
		
		try {
			while (resultado.next()) {
				formaPago = new FormaPago();
				formaPago.setMonto(resultado.getBigDecimal("MONTO"));
				formaPago.setFormaPago(resultado.getShort("FORMA_PAGO"));
				formaPago.setFormaPagoDesc(resultado.getString("FORMA_PAGO_DESC"));
				formaPago.setMoneCodigo(resultado.getShort("MONE_CODIGO"));
				formaPago.setdNumCheq(resultado.getString("NUMERO_CHEQUE"));
				formaPago.setdBcoEmi(resultado.getString("BANCO_EMISOR"));
				
				formasDePago.add(formaPago);
			}
		} catch (SQLException error) {
			try {
				Informe.ErrorLog("Error al convertir los datos, motivo: " + error.getMessage());
			} catch (IOException er) {
				er.printStackTrace();
			}
		}
		
		return formasDePago;
	}

}
