package converter;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import log.Informe;
import querys.LotesSifenQuery;

public class ObjectToLotesSifenQueryConverter {
	
	public static List<LotesSifenQuery> convertToLotesSifenQuery (ResultSet resultado){
		
		List<LotesSifenQuery> lotes = new ArrayList<LotesSifenQuery>();
		LotesSifenQuery lote = null;
		

		try {
			while (resultado.next()) {
				lote = new LotesSifenQuery();
				lote.setLosiCodigo(resultado.getLong("LOSI_CODIGO"));
				
				lotes.add(lote);
			
			}
		} catch (SQLException error) {
			try {
				Informe.ErrorLog("Error al convertir los datos, motivo: " + error.getMessage());
			} catch (IOException er) {
				er.printStackTrace();
			}
		}
		
		return lotes;
		
	}

}
