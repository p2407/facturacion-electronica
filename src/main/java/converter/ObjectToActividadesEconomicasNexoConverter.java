package converter;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import log.Informe;
import querys.ActividadesEconomicasNexoQuery;

public class ObjectToActividadesEconomicasNexoConverter {
	
	public static List<ActividadesEconomicasNexoQuery> convertToActividadesEconomicasNexoQuery (ResultSet resultado) {
		List<ActividadesEconomicasNexoQuery> actividades = new ArrayList<ActividadesEconomicasNexoQuery>();
		ActividadesEconomicasNexoQuery actividad = null;
		

		try {
			while (resultado.next()) {
				actividad = new ActividadesEconomicasNexoQuery();
				actividad.setcActEco(resultado.getString("COD_ACTIVIDAD"));
				actividad.setdDesActEco(resultado.getString("DESCRIPCION"));
				actividades.add(actividad);
			}
		} catch (SQLException ex) {
			try {
				Informe.ErrorLog("Error al convertir los datos en convertToActividadesEconomicasNexoQuery, motivo: " + ex.getMessage());
			} catch (IOException er) {
				er.printStackTrace();
			}
		}
		
		return actividades;
		
	}

}
