package converter;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import conexion.ObtenerDatosFacturasOracle;
import log.Informe;
import querys.ParametrosFacturasNexoQuery;


public class ObjectToParametrosFacturaNexoConverter {
	
	public static List<ParametrosFacturasNexoQuery> convertToParametrosFacturaNexoQuery (ResultSet resultado, Long losiCodigo) {
		List<ParametrosFacturasNexoQuery> facturas = new ArrayList<ParametrosFacturasNexoQuery>();
		ParametrosFacturasNexoQuery facturaCabecera = null;
		
		try {
			while (resultado.next()) {
				Boolean encolarLote = true;
				facturaCabecera = new ParametrosFacturasNexoQuery();
				facturaCabecera.setFaelCDC(resultado.getString("FAEL_CDC"));
				facturaCabecera.setdNumTim(resultado.getInt("NRO_TIMBRADO"));
				facturaCabecera.setdEst(resultado.getString("CODIGO_EST"));
				facturaCabecera.setdPunExp(resultado.getString("PUNTO_DE_EXPEDICION"));
				facturaCabecera.setdNumDoc(resultado.getString("NUMERO_DOCUMENTO"));
				facturaCabecera.setdSerieNum(resultado.getString("NUMERO_SERIE"));
				facturaCabecera.setdFeIniT(resultado.getString("FECHA_INICIO_VIGENCIA"));
				facturaCabecera.setdFeEmiDE(resultado.getString("FAEL_FECHA"));
				facturaCabecera.setdRucEm(resultado.getString("RUC_EMISOR"));
				facturaCabecera.setdDVEmi(resultado.getString("DV_EMISOR"));
				facturaCabecera.setdNomEmi(resultado.getString("NOMBRE_EMISOR"));
				facturaCabecera.setdDirEmi(resultado.getString("DIRECCION_EMISOR"));
				facturaCabecera.setdNumCas(resultado.getString("NUMERO_CASA_EMISOR"));
				facturaCabecera.setdDesCiuEmi(resultado.getString("CIUDAD_EMISOR"));
				facturaCabecera.setdTelEmi(resultado.getString("TELEFONO_EMISOR"));
				facturaCabecera.setdEmailE(resultado.getString("EMAIL_EMISOR"));
				facturaCabecera.setdNumIDRec(resultado.getString("NUM_ID_REC"));
				facturaCabecera.setdNomRec(resultado.getString("NOM_REC"));
				facturaCabecera.setiCondOpe(resultado.getShort("CONTADO"));
				facturaCabecera.setcMoneTiPag(resultado.getInt("FAEL_MONEDA"));
				facturaCabecera.setdEmailRec(resultado.getString("FAEL_CORREO_ELECTRONICO"));
				facturaCabecera.setdCelRec(resultado.getString("FAEL_NRO_TELEFONO"));
				
				facturaCabecera.setdTiCam(resultado.getBigDecimal("TIPO_CAMBIO_PARA_NO_PYG"));

				//Campo para las facturas a credito
				facturaCabecera.setdVencCuo(resultado.getString("FAEL_FCH_VENCIMIENTO"));
				facturaCabecera.setdMonCuota(resultado.getBigDecimal("FAEL_MONTO"));

				//DATOS DEL CDC
				facturaCabecera.setiTiDE(resultado.getShort("TIPO_DOCUMENTO"));
				facturaCabecera.setiTipEmi(resultado.getShort("TIPO_EMISION"));
				facturaCabecera.setiTipCont(resultado.getShort("TIPO_CONTRIBUYENTE"));
				facturaCabecera.setdCodSeg(resultado.getString("CODIGO_SEGURIDAD"));
				facturaCabecera.setdDVId(resultado.getString("DIGITO_VERIFICADOR"));
				facturaCabecera.setiTipTra(resultado.getShort("TIPO_DOCUMENTO_ELECTRONICO"));
				//CAMPO OBLIGATORIO PARA NOTA DE CREDITO
				facturaCabecera.setiMotEmi(resultado.getShort("MOTIVO_EMISION"));
				facturaCabecera.setiTipDocAso(resultado.getShort("TIPO_DOCUMENTO_ASOCIADO"));
				facturaCabecera.setdCdCDERef(resultado.getString("FAEL_CDC_ASOCIADO"));
				facturaCabecera.setdNTimDI(resultado.getString("NRO_TIMBRADO_ASOCIADO"));
				facturaCabecera.setdEstDocAso(resultado.getString("CODIGO_EST_ASOCIADO"));
				facturaCabecera.setdPExpDocAso(resultado.getString("PUNTO_DE_EXPEDICION_ASOCIADO"));
				facturaCabecera.setdNumDocAso(resultado.getString("NUMERO_DOCUMENTO_ASOCIADO"));
				facturaCabecera.setiTipoDocAso(resultado.getShort("TIPO_DOCUMENTO_IMPRESO"));
				facturaCabecera.setdFecEmiDI(resultado.getString("FECHA_DOCUMENTO_ASOCIADO"));
				//Campos del contribuyente
				if (resultado.getShort("PERS_CONTRIBUYENTE") == 1){
					String[] parts = resultado.getString("FAEL_RUC_CLIENTE").split("-");
					try {
						Informe.InfoLog(parts[0] + " " + parts[1]);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					facturaCabecera.setdRucRec(parts[0]);
					try {
						facturaCabecera.setdDVRec((short) Integer.parseInt(parts[1]));
					} catch (NullPointerException | NumberFormatException e) {
						// TODO Auto-generated catch block
						encolarLote = false;
						try {
							ObtenerDatosFacturasOracle.actualizaLotesRechazadosSifen(losiCodigo, resultado.getString("FAEL_CDC"), "Rechazado", "-20202", "Factura rechazada por la aplicacion por no poder formatear el ruc del cliente.");
						} catch (SQLException ex) {
							try {
								Informe.ErrorLog("Error en la actualizacion de los datos del lotes: " +losiCodigo+ ", motivo: " + ex);
							} catch (IOException er) {
								er.printStackTrace();
							}
						} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException ex) {
							try {
								Informe.ErrorLog("Error, motivo: " + ex);
							} catch (IOException er) {
								er.printStackTrace();
							}
						} catch (ClassCastException | IllegalArgumentException | NoClassDefFoundError | StackOverflowError ex) {
							try {
								Informe.ErrorLog("Error, motivo: " + ex);
							} catch (IOException er) {
								er.printStackTrace();
							}
						}
						//
						try {
							Informe.ErrorLog("No se pudo formatear el DV, motivo: " + e);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}else {
					facturaCabecera.setdRucRec(resultado.getString("FAEL_RUC_CLIENTE"));
				}
				facturaCabecera.setiNatRec(resultado.getShort("PERS_CONTRIBUYENTE"));
				facturaCabecera.setiTiContRec(resultado.getShort("PERS_TIPO"));
				
				if (encolarLote)
					facturas.add(facturaCabecera);
				
			}
		} catch (SQLException error) {
			try {
				Informe.ErrorLog("Error al convertir los datos, motivo: " + error.getMessage());
			} catch (IOException er) {
				er.printStackTrace();
			}
		}
		
		return facturas;
	}
	
	public static List<ParametrosFacturasNexoQuery> convertToFacturasInvalidasQuery (ResultSet resultado) {
		List<ParametrosFacturasNexoQuery> facturas = new ArrayList<ParametrosFacturasNexoQuery>();
		ParametrosFacturasNexoQuery facturaCabecera = null;
		
		try {
			while (resultado.next()) {
				facturaCabecera = new ParametrosFacturasNexoQuery();
				facturaCabecera.setFaelCDC(resultado.getString("FAEL_CDC"));
				
				facturas.add(facturaCabecera);
				
			}
		} catch (SQLException error) {
			try {
				Informe.ErrorLog("Error al convertir los datos, motivo: " + error.getMessage());
			} catch (IOException er) {
				er.printStackTrace();
			}
		}
		
		return facturas;
	}

}
